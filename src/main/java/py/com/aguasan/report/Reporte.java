package py.com.aguasan.report;

import java.util.List;


import py.com.aguasan.util.ManejoDeArchivos;

public class Reporte {

	private String fileName;

	private String filePath;

	public Reporte(String fileName) {
		super();
		this.fileName = fileName;
	}

	// representa a los rows(filas) del reporte
	private List<ReporteRow> rows;

	
	public String toCSV() throws Exception {
		ManejoDeArchivos manejoDeArchivos = new ManejoDeArchivos();

		return manejoDeArchivos.toCSV(this);
	
	}

	public List<ReporteRow> getRows() {
		return rows;
	}

	public void setRows(List<ReporteRow> rows) {
		this.rows = rows;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getFileName() {
		return fileName;
	}

}
