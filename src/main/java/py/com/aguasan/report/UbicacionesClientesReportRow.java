package py.com.aguasan.report;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

import py.com.aguasan.dto.UbicacionClienteDTO;
import py.com.aguasan.entity.UbicacionCliente;

public class UbicacionesClientesReportRow extends ReporteRow{

	@CsvBindByName(column = "Cod.Ubicacion")
	@CsvBindByPosition(position = 0)
	private Long codUbicacion;
	
	@CsvBindByName(column = "Tipo Ubicacion")
	@CsvBindByPosition(position = 1)
	private String tipoUbicacion;
	
	@CsvBindByName(column = "Cod.Cliente")
	@CsvBindByPosition(position = 2)
	private String codigoCliente;
	
	
	@CsvBindByName(column = "Cliente")
	@CsvBindByPosition(position = 3)
	private String nombreCliente;
	
	@CsvBindByName(column = "Latitud")
	@CsvBindByPosition(position = 4)
	private String latitud;
	
	@CsvBindByName(column = "Longitud")
	@CsvBindByPosition(position = 5)
	private String longitud;
	
	
	@CsvBindByName(column = "Referencia")
	@CsvBindByPosition(position = 6)
	private String referencia;
	
	
	@CsvBindByName(column = "Fec.Alta")
	@CsvBindByPosition(position = 7)
	private String fecAlta;
	
	
	@CsvBindByName(column = "Usr.Alta")
	@CsvBindByPosition(position = 8)
	private String usrAlta;
	
	@CsvBindByName(column = "Fec.Ult.Act")
	@CsvBindByPosition(position = 9)
	private String fecUltAct;
	
	
	@CsvBindByName(column = "Usr.Ult.Act")
	@CsvBindByPosition(position = 10)
	private String usrUltAct;
	
	public UbicacionesClientesReportRow(UbicacionClienteDTO ubicacion) {
		this.codUbicacion = ubicacion.getCodUbicacion();
		this.tipoUbicacion = ubicacion.getTipoUbicacion().getTipoUbicacion().toString();
		this.codigoCliente = ubicacion.getCliente().getCodCliente().toString();
		this.nombreCliente = ubicacion.getCliente().getNombres() + " " + ubicacion.getCliente().getApellidos();
		this.latitud = ubicacion.getLatitud();
		this.longitud = ubicacion.getLongitud();
		this.referencia = ubicacion.getReferencia();
		this.fecAlta = ubicacion.getFecAlta();
		this.usrAlta = ubicacion.getUsrAlta();
		this.fecUltAct = ubicacion.getFecUltAct();
		this.usrUltAct = ubicacion.getUsrUltAct();
		
	}

	public Long getCodUbicacion() {
		return codUbicacion;
	}

	public void setCodUbicacion(Long codUbicacion) {
		this.codUbicacion = codUbicacion;
	}

	public String getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(String tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	@Override
	public String toString() {
		return "UbicacionesClientesReport [codUbicacion=" + codUbicacion + ", tipoUbicacion=" + tipoUbicacion
				+ ", codigoCliente=" + codigoCliente + ", nombreCliente=" + nombreCliente + ", latitud=" + latitud
				+ ", longitud=" + longitud + ", referencia=" + referencia + ", fecAlta=" + fecAlta + ", usrAlta="
				+ usrAlta + ", fecUltAct=" + fecUltAct + ", usrUltAct=" + usrUltAct + "]";
	}

}
