package py.com.aguasan.report;

import java.math.BigInteger;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class FormulariosReportRow extends ReporteRow {

	@CsvBindByName(column = "Nro.Formulario")
	@CsvBindByPosition(position = 0)
	private BigInteger nroFormulario;

	@CsvBindByName(column = "Cod.Cliente")
	@CsvBindByPosition(position = 1)
	private Long codCliente;

	@CsvBindByName(column = "Nombre")
	@CsvBindByPosition(position = 2)
	private String nombreCliente;

	@CsvBindByName(column = "Apellido")
	@CsvBindByPosition(position = 3)
	private String apellidoCliente;

	@CsvBindByName(column = "Bidones")
	@CsvBindByPosition(position = 4)
	private int bidones;

	@CsvBindByName(column = "Dispensers")
	@CsvBindByPosition(position = 5)
	private int dispensers;

	@CsvBindByName(column = "Bebederos")
	@CsvBindByPosition(position = 6)
	private int bebederos;

	@CsvBindByName(column = "Fec.Alta")
	@CsvBindByPosition(position = 7)
	private String fecAlta;

	@CsvBindByName(column = "Usr.Alta")
	@CsvBindByPosition(position = 8)
	private String usrAlta;

	@CsvBindByName(column = "Fec.Ult.Act")
	@CsvBindByPosition(position = 9)
	private String fecUltAct;

	@CsvBindByName(column = "Usr.Ult.Act")
	@CsvBindByPosition(position = 10)
	private String usrUltAct;

	public BigInteger getNroFormulario() {
		return nroFormulario;
	}

	public void setNroFormulario(BigInteger nroFormulario) {
		this.nroFormulario = nroFormulario;
	}

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public int getBidones() {
		return bidones;
	}

	public void setBidones(int bidones) {
		this.bidones = bidones;
	}

	public int getDispensers() {
		return dispensers;
	}

	public void setDispensers(int dispensers) {
		this.dispensers = dispensers;
	}

	public int getBebederos() {
		return bebederos;
	}

	public void setBebederos(int bebederos) {
		this.bebederos = bebederos;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public String getApellidoCliente() {
		return apellidoCliente;
	}

	public void setApellidoCliente(String apellidoCliente) {
		this.apellidoCliente = apellidoCliente;
	}

	@Override
	public String toString() {
		return "FormulariosReport [nroFormulario=" + nroFormulario + ", codCliente=" + codCliente + ", nombreCliente="
				+ nombreCliente + ", bidones=" + bidones + ", dispensers=" + dispensers + ", bebederos=" + bebederos
				+ ", fecAlta=" + fecAlta + ", usrAlta=" + usrAlta + ", fecUltAct=" + fecUltAct + ", usrUltAct="
				+ usrUltAct + "]";
	}

}
