package py.com.aguasan.report;

import java.math.BigInteger;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class VisitasReportRow extends ReporteRow{
	@CsvBindByName(column = "Nro.Visita")
	@CsvBindByPosition(position = 0)
	private Long nroVisita;
	
	
	@CsvBindByName(column = "Cod.Cliente")
	@CsvBindByPosition(position = 1)
	private Long codCliente;
	
	@CsvBindByName(column = "Cliente")
	@CsvBindByPosition(position = 2)
	private String nombre;
	
	@CsvBindByName(column = "Situacion Visita")
	@CsvBindByPosition(position = 3)
	private String situacionVisita;
	
	
	@CsvBindByName(column = "Bidones Canje")
	@CsvBindByPosition(position = 4)
	private BigInteger bidonesCanje;
	
	
	@CsvBindByName(column = "Bidones")
	@CsvBindByPosition(position = 5)
	private BigInteger bidones;
	
	@CsvBindByName(column = "Fec.Alta")
	@CsvBindByPosition(position = 6)
	private String fecAlta;
	
	
	@CsvBindByName(column = "Usr.Alta")
	@CsvBindByPosition(position = 7)
	private String usrAlta;
	
	@CsvBindByName(column = "Fec.Ult.Act")
	@CsvBindByPosition(position = 8)
	private String fecUltAct;
	
	@CsvBindByName(column = "Usr.Ult.Act")
	@CsvBindByPosition(position = 9)
	private String usrUltAct;

	public Long getNroVisita() {
		return nroVisita;
	}

	public void setNroVisita(Long nroVisita) {
		this.nroVisita = nroVisita;
	}

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSituacionVisita() {
		return situacionVisita;
	}

	public void setSituacionVisita(String situacionVisita) {
		this.situacionVisita = situacionVisita;
	}

	public BigInteger getBidonesCanje() {
		return bidonesCanje;
	}

	public void setBidonesCanje(BigInteger bidonesCanje) {
		this.bidonesCanje = bidonesCanje;
	}

	public BigInteger getBidones() {
		return bidones;
	}

	public void setBidones(BigInteger bidones) {
		this.bidones = bidones;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	@Override
	public String toString() {
		return "VisitasReport [nroVisita=" + nroVisita + ", codCliente=" + codCliente + ", nombre=" + nombre
				+ ", situacionVisita=" + situacionVisita + ", bidonesCanje=" + bidonesCanje + ", bidones=" + bidones
				+ ", fecAlta=" + fecAlta + ", usrAlta=" + usrAlta + ", fecUltAct=" + fecUltAct + ", usrUltAct="
				+ usrUltAct + "]";
	}

}
