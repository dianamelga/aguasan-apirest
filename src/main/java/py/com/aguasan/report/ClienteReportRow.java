package py.com.aguasan.report;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class ClienteReportRow extends ReporteRow {
	@CsvBindByName(column = "Cod.Cliente")
	@CsvBindByPosition(position = 0)
	private Long codCliente;

	@CsvBindByName(column = "Cliente")
	@CsvBindByPosition(position = 1)
	private String nombreCliente;

	@CsvBindByName(column = "Celular")
	@CsvBindByPosition(position = 2)
	private String celular;

	@CsvBindByName(column = "Telefono")
	@CsvBindByPosition(position = 3)
	private String telefono;

	@CsvBindByName(column = "Tipo Documento")
	@CsvBindByPosition(position = 4)
	private String tipoDocumento;

	@CsvBindByName(column = "Nro.Documento")
	@CsvBindByPosition(position = 5)
	private String nroDocumento;

	@CsvBindByName(column = "Ciudad")
	@CsvBindByPosition(position = 6)
	private String ciudad;

	@CsvBindByName(column = "Barrio")
	@CsvBindByPosition(position = 7)
	private String barrio;

	@CsvBindByName(column = "Direccion")
	@CsvBindByPosition(position = 8)
	private String direccion;

	@CsvBindByName(column = "Observacion")
	@CsvBindByPosition(position = 9)
	private String observacion;

	@CsvBindByName(column = "Dias Visita")
	@CsvBindByPosition(position = 10)
	private String diasVisita;

	@CsvBindByName(column = "Nro.Formulario")
	@CsvBindByPosition(position = 11)
	private String formulario;

	@CsvBindByName(column = "Bidones")
	@CsvBindByPosition(position = 12)
	private int bidones;

	@CsvBindByName(column = "Dispensers")
	@CsvBindByPosition(position = 13)
	private int dispensers;

	@CsvBindByName(column = "Bebederos")
	@CsvBindByPosition(position = 14)
	private int bebederos;

	@CsvBindByName(column = "Activo")
	@CsvBindByPosition(position = 15)
	private String activo;

	@CsvBindByName(column = "Fec.Alta")
	@CsvBindByPosition(position = 16)
	private String fechaAlta;

	@CsvBindByName(column = "Usr.Alta")
	@CsvBindByPosition(position = 17)
	private String userAlta;

	@CsvBindByName(column = "Fec.Ult.Act")
	@CsvBindByPosition(position = 18)
	private String fechaUltimaAct;

	@CsvBindByName(column = "Usr.Ult.Act")
	@CsvBindByPosition(position = 19)
	private String userUltimaAct;

	@CsvBindByName(column = "Latitud")
	@CsvBindByPosition(position = 20)
	private String latitud;

	@CsvBindByName(column = "Longitud")
	@CsvBindByPosition(position = 21)
	private String longitud;

	public int getBidones() {
		return bidones;
	}

	public void setBidones(int bidones) {
		this.bidones = bidones;
	}

	public int getDispensers() {
		return dispensers;
	}

	public void setDispensers(int dispensers) {
		this.dispensers = dispensers;
	}

	public int getBebederos() {
		return bebederos;
	}

	public void setBebederos(int bebederos) {
		this.bebederos = bebederos;
	}

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}



	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getDiasVisita() {
		return diasVisita;
	}

	public void setDiasVisita(String diasVisita) {
		this.diasVisita = diasVisita;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getUserAlta() {
		return userAlta;
	}

	public void setUserAlta(String userAlta) {
		this.userAlta = userAlta;
	}

	public String getFechaUltimaAct() {
		return fechaUltimaAct;
	}

	public void setFechaUltimaAct(String fechaUltimaAct) {
		this.fechaUltimaAct = fechaUltimaAct;
	}

	public String getUserUltimaAct() {
		return userUltimaAct;
	}

	public void setUserUltimaAct(String userUltimaAct) {
		this.userUltimaAct = userUltimaAct;
	}

	public String getFormulario() {
		return formulario;
	}

	public void setFormulario(String formulario) {
		this.formulario = formulario;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	@Override
	public String toString() {
		return "ClienteReporteRow [codCliente=" + codCliente + ", nombres=" + nombreCliente
				+ ", celular=" + celular + ", telefono=" + telefono + ", tipoDocumento=" + tipoDocumento
				+ ", nroDocumento=" + nroDocumento + ", ciudad=" + ciudad + ", barrio=" + barrio + ", direccion="
				+ direccion + ", observacion=" + observacion + ", diasVisita=" + diasVisita + ", activo=" + activo
				+ ", fechaAlta=" + fechaAlta + ", userAlta=" + userAlta + ", fechaUltimaAct=" + fechaUltimaAct
				+ ", userUltimaAct=" + userUltimaAct + ", formulario=" + formulario + "]";
	}

}
