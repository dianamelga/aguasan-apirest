package py.com.aguasan.jobs;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.google.gson.Gson;
import py.com.aguasan.dto.EventoDTO;
import py.com.aguasan.entity.AguaSanMessage;
import py.com.aguasan.service.IEventoService;
import py.com.aguasan.service.IFirebaseAdminService;
import py.com.aguasan.util.FechaUtil;

import java.lang.reflect.Type;
import com.google.gson.reflect.TypeToken;

/*
 * Esta clase se encarga de ejecutar tareas programadas (o jobs)
 */


@Configuration
@EnableScheduling
public class AguaSanJob {
	
	private Long lastEventSent; 
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AguaSanJob.class);

	@Autowired
	private IFirebaseAdminService iFirebaseAdminService;
	@Autowired
	private IEventoService iEventoService;

	@Scheduled(fixedRate = 30000) //se ejecuta cada 30 segundos
    public void executeTask1() {
		try {
			FechaUtil fechaUtil = new FechaUtil();
			String resultSet = iEventoService.getEventos("PEND", fechaUtil.getDateWithoutTimeUsingCalendar());
			Type listType = new TypeToken<ArrayList<EventoDTO>>(){}.getType();
			List<EventoDTO> eventos = new Gson().fromJson(resultSet, listType);
			
			for(EventoDTO evento : eventos) { 
				if(lastEventSent != null && evento.getNroEvento() <= lastEventSent) {
					continue;
				}
				AguaSanMessage message = new AguaSanMessage();
				message.setTitle("Notificacion");
				message.setMessage(evento.getObservacion());
				iFirebaseAdminService.sendPushNotification(message);
				LOGGER.info("Mensaje enviado: "+message.toString());
				lastEventSent = evento.getNroEvento();
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
		}
        
    }
	
	
	
}
