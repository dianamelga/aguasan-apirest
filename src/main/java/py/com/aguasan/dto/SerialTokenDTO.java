package py.com.aguasan.dto;

import java.math.BigInteger;

public class SerialTokenDTO {
	public BigInteger serial;
	public String token;
	public boolean isAdmin;
	public String mail;

	public SerialTokenDTO(BigInteger serial, String token, boolean isAdmin, String mail) {
		super();
		this.serial = serial;
		this.token = token;
		this.isAdmin = isAdmin;
		this.mail = mail;
	}

	

	public String getMail() {
		return mail;
	}



	public void setMail(String mail) {
		this.mail = mail;
	}



	public BigInteger getSerial() {
		return serial;
	}

	public void setSerial(BigInteger serial) {
		this.serial = serial;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

}
