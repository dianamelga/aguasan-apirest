package py.com.aguasan.dto;

import java.math.BigInteger;

import py.com.aguasan.entity.SolicitudAccion;
import py.com.aguasan.util.FechaUtil;

public class SolicitudAccionDTO {

	private BigInteger idAccion;

	private String tipo;

	private String objetoJson;

	private String estado;

	private String idRemoto;

	private String serialInstal;

	private String fechaAlta;

	private UsuarioDTO usuarioAlta;

	private String fechaUltAct;

	private UsuarioDTO usuarioUltAct;

	public BigInteger getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(BigInteger idAccion) {
		this.idAccion = idAccion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getObjetoJson() {
		return objetoJson;
	}

	public void setObjetoJson(String objetoJson) {
		this.objetoJson = objetoJson;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIdRemoto() {
		return idRemoto;
	}

	public void setIdRemoto(String idRemoto) {
		this.idRemoto = idRemoto;
	}

	public String getSerialInstal() {
		return serialInstal;
	}

	public void setSerialInstal(String serialInstal) {
		this.serialInstal = serialInstal;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public UsuarioDTO getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(UsuarioDTO usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public String getFechaUltAct() {
		return fechaUltAct;
	}

	public void setFechaUltAct(String fechaUltAct) {
		this.fechaUltAct = fechaUltAct;
	}

	public UsuarioDTO getUsuarioUltAct() {
		return usuarioUltAct;
	}

	public void setUsuarioUltAct(UsuarioDTO usuarioUltAct) {
		this.usuarioUltAct = usuarioUltAct;
	}

	public SolicitudAccion toEntity() {
		SolicitudAccion solicitudAccion = new SolicitudAccion();

		solicitudAccion.setIdAccion(this.idAccion);
		solicitudAccion.setTipo(this.tipo);
		solicitudAccion.setObjetoJson(this.objetoJson);
		solicitudAccion.setEstado(this.estado);
		solicitudAccion.setIdRemoto(new BigInteger(this.idRemoto));
		solicitudAccion.setSerialInstal(new BigInteger(this.serialInstal));
		solicitudAccion.setFechaAlta(new FechaUtil().stringToDateParser(this.fechaAlta));
		solicitudAccion.setUsuarioAlta(this.usuarioAlta.toEntity());
		solicitudAccion.setFechaUltAct(new FechaUtil().stringToDateParser(this.fechaUltAct));
		solicitudAccion.setUsuarioUltAct(this.usuarioUltAct.toEntity());

		return solicitudAccion;
	}

}
