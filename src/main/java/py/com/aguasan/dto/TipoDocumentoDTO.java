package py.com.aguasan.dto;

import py.com.aguasan.entity.TipoDocumento;

public class TipoDocumentoDTO {

	private String codTipoDocumento;

	private String tipoDocumento;

	public String getCodTipoDocumento() {
		return codTipoDocumento;
	}

	public void setCodTipoDocumento(String codTipoDocumento) {
		this.codTipoDocumento = codTipoDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public TipoDocumento toEntity() {
		TipoDocumento tipoDocumento = new TipoDocumento();
		tipoDocumento.setCodTipoDocumento(this.codTipoDocumento);
		tipoDocumento.setTipoDocumento(this.tipoDocumento);
		return tipoDocumento;
	}

}
