package py.com.aguasan.dto;

import py.com.aguasan.entity.Barrio;

public class BarrioDTO {

	private String codBarrio;

	private String barrio;

	private CiudadDTO ciudad;

	public String getCodBarrio() {
		return codBarrio;
	}

	public void setCodBarrio(String codBarrio) {
		this.codBarrio = codBarrio;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public CiudadDTO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadDTO ciudad) {
		this.ciudad = ciudad;
	}

	public Barrio toEntity() {
		Barrio barrio = new Barrio();
		barrio.setCodBarrio(this.codBarrio);
		barrio.setBarrio(this.barrio.toUpperCase());
		barrio.setCiudad(this.ciudad.toEntity());
		return barrio;
	}

}
