package py.com.aguasan.dto;

import py.com.aguasan.entity.Usuario;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public class UsuarioDTO {

	private String userName;

	private String password;

	private String nombre;

	private TipoDocumentoDTO tipoDocumento;

	private String nroDocumento;

	private String email;

	private String isAdmin;

	private String activo;

	private String fecAlta;

	private String fecUltAct;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIsAdmin() {
		return isAdmin;
	}

	public void setIsAdmin(String isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public Usuario toEntity() {
		Usuario usuario = new Usuario();
		usuario.setUserName(this.userName);
		usuario.setPassword(this.password);
		usuario.setNombre(this.nombre);
		usuario.setTipoDocumento(this.tipoDocumento.toEntity());
		usuario.setNroDocumento(this.nroDocumento);
		usuario.setEmail(this.email);
		usuario.setEsAdmin(this.isAdmin);
		usuario.setActivo(this.activo);
		usuario.setFecAlta(new FechaUtil().stringToDateParser(this.fecAlta));
		usuario.setFecUltAct(new FechaUtil().stringToDateParser(this.fecUltAct));
		return usuario;
	}
}