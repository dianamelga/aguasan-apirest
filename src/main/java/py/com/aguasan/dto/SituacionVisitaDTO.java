package py.com.aguasan.dto;

import java.math.BigInteger;

import py.com.aguasan.entity.SituacionVisita;

public class SituacionVisitaDTO {

	public String codSituacion;

	public String situacion;

	public String getCodSituacion() {
		return codSituacion;
	}

	public void setCodSituacion(String codSituacion) {
		this.codSituacion = codSituacion;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public SituacionVisita toEntity() {
		SituacionVisita situacionVisita = new SituacionVisita();
		situacionVisita.setCodSituacion(new BigInteger(this.codSituacion));
		situacionVisita.setSituacion(this.situacion);
		return situacionVisita;
	}

}
