package py.com.aguasan.dto;

import java.util.ArrayList;
import java.util.List;

import py.com.aguasan.entity.Evento;
import py.com.aguasan.util.FechaUtil;

public class EventoDTO {

	private Long nroEvento;

	private Long codCliente;

	private String fecEvento;

	private String observacion;

	private String estado;

	private String fecAlta;

	private String usrAlta;

	private String fecUltAct;

	private String usrUltAct;

	public Long getNroEvento() {
		return nroEvento;
	}

	public void setNroEvento(Long nroEvento) {
		this.nroEvento = nroEvento;
	}

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public String getFecEvento() {
		return fecEvento;
	}

	public void setFecEvento(String fecEvento) {
		this.fecEvento = fecEvento;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public Evento toEntity() {
		Evento evento = new Evento();
		evento.setCodCliente(this.codCliente);
		evento.setNroEvento(this.nroEvento);
		evento.setFecEvento(new FechaUtil().stringToDateParser(this.fecEvento));
		evento.setEstado(this.estado);
		evento.setObservacion(this.observacion);
		evento.setFecAlta(new FechaUtil().stringToDateParser(this.fecAlta));
		evento.setUsrAlta(this.usrAlta);
		evento.setFecUltAct(new FechaUtil().stringToDateParser(this.fecUltAct));
		evento.setUsrUltAct(this.usrUltAct);

		return evento;
	}

	public List<Evento> toEntityList(List<EventoDTO> eventosDTO) {
		List<Evento> eventos = new ArrayList<Evento>();
		for (EventoDTO eventoDTO : eventosDTO) {
			eventos.add(eventoDTO.toEntity());
		}
		return eventos;
	}
}
