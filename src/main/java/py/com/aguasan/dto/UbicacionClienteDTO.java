package py.com.aguasan.dto;

import java.text.ParseException;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.aguasan.entity.UbicacionCliente;
import py.com.aguasan.entity.UbicacionTipo;
import py.com.aguasan.util.FechaUtil;

/**
 * 
 * @author Diana Raquel Melgarejo Velgara
 *
 */

public class UbicacionClienteDTO {

	private Long codUbicacion;
	private UbicacionTipo tipoUbicacion;
	@JsonIgnore
	private ClienteDTO cliente;
	private String latitud;
	private String longitud;

	// hay que parsearlo a un base64
	private String foto;

	private String referencia;

	private String fecAlta;
	private String usrAlta;
	private String fecUltAct;
	private String usrUltAct;

	public Long getCodUbicacion() {
		return codUbicacion;
	}

	public void setCodUbicacion(Long codUbicacion) {
		this.codUbicacion = codUbicacion;
	}

	public UbicacionTipo getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(UbicacionTipo tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public UbicacionCliente toEntity() {
		UbicacionCliente entity = new UbicacionCliente();
		entity.setCliente(null);
		entity.setTipoUbicacion(this.tipoUbicacion);
		entity.setCodUbicacion(this.codUbicacion);
		entity.setLatitud(this.latitud);
		entity.setLongitud(this.longitud);
		entity.setReferencia(this.referencia.replace("'", ""));
		try {
			entity.setFecAlta(new FechaUtil().formatoFechaHora(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			entity.setFecAlta(new FechaUtil().stringToDateParser(this.fecAlta));

		}
		entity.setUsrAlta(this.usrAlta);
		try {
			entity.setFecUltAct(new FechaUtil().formatoFechaHora(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			entity.setFecUltAct(new FechaUtil().stringToDateParser(this.fecUltAct));
		}
		entity.setUsrUltAct(this.usrUltAct);

		return entity;
	}
}
