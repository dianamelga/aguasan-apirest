package py.com.aguasan.dto;

import py.com.aguasan.entity.Ciudad;

public class CiudadDTO {

	private String codCiudad;

	private String ciudad;

	public String getCodCiudad() {
		return codCiudad;
	}

	public void setCodCiudad(String codCiudad) {
		this.codCiudad = codCiudad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Ciudad toEntity() {
		Ciudad ciudad = new Ciudad();
		ciudad.setCodCiudad(this.codCiudad);
		ciudad.setCiudad(this.ciudad.toUpperCase());
		return ciudad;
	}
}
