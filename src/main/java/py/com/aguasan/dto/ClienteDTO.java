package py.com.aguasan.dto;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

import py.com.aguasan.entity.Cliente;
import py.com.aguasan.entity.UbicacionCliente;
import py.com.aguasan.util.FechaUtil;

public class ClienteDTO {

	private Long codCliente;

	private String nombres;

	private String apellidos;

	private String celular;

	private String telefono;

	private TipoDocumentoDTO tipoDocumento;

	private String nroDocumento;

	private CiudadDTO ciudad;

	private BarrioDTO barrio;

	private String direccion;

	private String observacion;

	private List<String> diasVisita;

	private List<UbicacionClienteDTO> ubicaciones;

	private String activo;

	private String fechaAlta;

	private String userAlta;

	private String fechaUltimaAct;

	private String userUltimaAct;

	// ultimo formulario del cliente
	private FormularioDTO formulario;

	private List<FormularioDTO> formularios;

	private List<VisitaDTO> visitas;

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public TipoDocumentoDTO getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumentoDTO tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public CiudadDTO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadDTO ciudad) {
		this.ciudad = ciudad;
	}

	public BarrioDTO getBarrio() {
		return barrio;
	}

	public void setBarrio(BarrioDTO barrio) {
		this.barrio = barrio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public List<String> getDiasVisita() {
		return diasVisita;
	}

	public void setDiasVisita(List<String> diasVisita) {
		this.diasVisita = diasVisita;
	}

	public List<UbicacionClienteDTO> getUbicaciones() {
		return ubicaciones;
	}

	public void setUbicaciones(List<UbicacionClienteDTO> ubicaciones) {
		this.ubicaciones = ubicaciones;
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getUserAlta() {
		return userAlta;
	}

	public void setUserAlta(String userAlta) {
		this.userAlta = userAlta;
	}

	public String getFechaUltimaAct() {
		return fechaUltimaAct;
	}

	public void setFechaUltimaAct(String fechaUltimaAct) {
		this.fechaUltimaAct = fechaUltimaAct;
	}

	public String getUserUltimaAct() {
		return userUltimaAct;
	}

	public void setUserUltimaAct(String userUltimaAct) {
		this.userUltimaAct = userUltimaAct;
	}

	public FormularioDTO getFormulario() {
		return formulario;
	}

	public void setFormulario(FormularioDTO formulario) {
		this.formulario = formulario;
	}

	public List<FormularioDTO> getFormularios() {
		return formularios;
	}

	public void setFormularios(List<FormularioDTO> formularios) {
		this.formularios = formularios;
	}

	public List<VisitaDTO> getVisitas() {
		return visitas;
	}

	public void setVisitas(List<VisitaDTO> visitas) {
		this.visitas = visitas;
	}

	public Cliente toEntity() {
		Cliente cliente = new Cliente();
		cliente.setCodCliente(this.codCliente);
		cliente.setNombres(this.nombres);
		cliente.setApellidos(this.apellidos);
		cliente.setCelular(this.celular);
		cliente.setTelefono(this.telefono);
		cliente.setTipoDocumento(this.tipoDocumento.toEntity());
		cliente.setNroDocumento(this.nroDocumento);
		cliente.setBarrio(this.barrio.toEntity());
		cliente.setDireccion(this.direccion);
		cliente.setObservacion(this.observacion);
		cliente.setDiasVisita((new Gson()).toJson(this.diasVisita));
		cliente.setActivo(this.activo);
		cliente.setFechaAlta(new FechaUtil().stringToDateParser(this.fechaAlta));
		cliente.setUserAlta(this.userAlta);
		try {
			cliente.setFechaUltimaAct(new FechaUtil().formatoFechaHora(new Date()));
		} catch (ParseException e) {
			e.printStackTrace();
			cliente.setFechaUltimaAct(new FechaUtil().stringToDateParser(this.fechaUltimaAct));
		}
		cliente.setUserUltimaAct(this.userUltimaAct);
		return cliente;
	}

	/**
	 * para obtener lostado de ubicaciones en forma de entity
	 * 
	 * @return List<UbicacionCliente>
	 */
	public List<UbicacionCliente> getListUbicacionesClienteDTOtoEntity() {
		List<UbicacionCliente> ubicacionesEntity = new ArrayList<UbicacionCliente>();
		for (UbicacionClienteDTO ubicacionCliente : this.ubicaciones) {
			ubicacionesEntity.add(ubicacionCliente.toEntity());
		}
		return ubicacionesEntity;
	}

	public void setListUbicacionesClienteDTO(List<UbicacionCliente> listaUbicacionesEntity) {
		List<UbicacionClienteDTO> ubicacionesDTO = new ArrayList<UbicacionClienteDTO>();
		for (UbicacionCliente ubicacionCliente : listaUbicacionesEntity) {
			ubicacionesDTO.add(ubicacionCliente.toDTO());
		}
		this.ubicaciones = ubicacionesDTO;
	}

	@Override
	public String toString() {
		return "ClienteDTO [codCliente=" + codCliente + ", nombres=" + nombres + ", apellidos=" + apellidos
				+ ", celular=" + celular + ", telefono=" + telefono + ", tipoDocumento=" + tipoDocumento
				+ ", nroDocumento=" + nroDocumento + ", ciudad=" + ciudad + ", barrio=" + barrio + ", direccion="
				+ direccion + ", observacion=" + observacion + ", diasVisita=" + diasVisita + ", ubicaciones="
				+ ubicaciones + ", activo=" + activo + ", fechaAlta=" + fechaAlta + ", userAlta=" + userAlta
				+ ", fechaUltimaAct=" + fechaUltimaAct + ", userUltimaAct=" + userUltimaAct + "]";
	}

}
