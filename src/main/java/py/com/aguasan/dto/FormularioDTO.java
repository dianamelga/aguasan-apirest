package py.com.aguasan.dto;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.Date;

import py.com.aguasan.entity.Formulario;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
public class FormularioDTO {

	private BigInteger nroFormulario;
	private ClienteDTO codCliente;
	private int bidones;
	private int dispensers;
	private int bebederos;
	private String fecAlta;
	private String usrAlta;
	private String fecUltAct;
	private String usrUltAct;

	public FormularioDTO() {
		super();
		this.nroFormulario = BigInteger.ZERO;
	}

	public BigInteger getNroFormulario() {
		return nroFormulario;
	}

	public void setNroFormulario(BigInteger nroFormulario) {
		this.nroFormulario = nroFormulario;
	}

	public ClienteDTO getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(ClienteDTO codCliente) {
		this.codCliente = codCliente;
	}

	public int getBidones() {
		return bidones;
	}

	public void setBidones(int bidones) {
		this.bidones = bidones;
	}

	public int getDispensers() {
		return dispensers;
	}

	public void setDispensers(int dispensers) {
		this.dispensers = dispensers;
	}

	public int getBebederos() {
		return bebederos;
	}

	public void setBebederos(int bebederos) {
		this.bebederos = bebederos;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public Formulario toEntity() {
		Formulario formulario = new Formulario();
		formulario.setNroFormulario(this.nroFormulario);
		formulario.setCodCliente(this.codCliente.toEntity());
		formulario.setBidones(this.bidones);
		formulario.setDispensers(this.dispensers);
		formulario.setBebederos(this.bebederos);
		formulario.setFecAlta(new FechaUtil().stringToDateParser(this.fecAlta));
		formulario.setUsrAlta(this.usrAlta);
		try {
			formulario.setFecUltAct(new FechaUtil().formatoFechaHora(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			formulario.setFecUltAct(new FechaUtil().stringToDateParser(this.fecUltAct));
		}
		formulario.setUsrUltAct(this.usrUltAct);
		return formulario;
	}

}
