package py.com.aguasan.dto;

import java.math.BigInteger;

import py.com.aguasan.entity.UbicacionTipo;
import py.com.aguasan.util.FechaUtil;

/**
 * 
 * @author Diana Raquel Melgarejo Velgara
 *
 */
public class UbicacionTipoDTO {
	private BigInteger codTipoUbicacion;
	private String tipoUbicacion;
	private String fecAlta;
	private String usrAlta;
	private String fecUltAct;
	private String usrUltAct;

	public BigInteger getCodTipoUbicacion() {
		return codTipoUbicacion;
	}

	public void setCodTipoUbicacion(BigInteger codTipoUbicacion) {
		this.codTipoUbicacion = codTipoUbicacion;
	}

	public String getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(String tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public UbicacionTipo toEntity() {
		UbicacionTipo entity = new UbicacionTipo();
		entity.setCodTipoUbicacion(this.codTipoUbicacion);
		entity.setTipoUbicacion(this.tipoUbicacion);
		entity.setFecAlta(new FechaUtil().stringToDateParser(this.fecAlta));
		entity.setUsrAlta(this.usrAlta);
		entity.setFecUltAct(new FechaUtil().stringToDateParser(this.fecUltAct));
		entity.setUsrUltAct(this.usrUltAct);

		return entity;
	}

}
