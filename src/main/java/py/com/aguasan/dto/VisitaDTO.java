package py.com.aguasan.dto;

import java.math.BigInteger;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import py.com.aguasan.entity.Visita;
import py.com.aguasan.util.FechaUtil;

public class VisitaDTO {

	private Long nroVisita;

	private ClienteDTO cliente;

	private SituacionVisitaDTO situacion;

	private String bidonesCanje;

	private String bidones;

	private String fecAlta;

	private String usrAlta;

	private String fecUltAct;

	private String usrUltAct;

	public Long getNroVisita() {
		return nroVisita;
	}

	public void setNroVisita(Long nroVisita) {
		this.nroVisita = nroVisita;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public SituacionVisitaDTO getSituacion() {
		return situacion;
	}

	public void setSituacion(SituacionVisitaDTO situacion) {
		this.situacion = situacion;
	}

	public String getBidonesCanje() {
		return bidonesCanje;
	}

	public void setBidonesCanje(String bidonesCanje) {
		this.bidonesCanje = bidonesCanje;
	}

	public String getBidones() {
		return bidones;
	}

	public void setBidones(String bidones) {
		this.bidones = bidones;
	}

	public String getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(String fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public String getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(String fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public Visita toEntity() {
		Visita visita = new Visita();
		visita.setNroVisita(this.nroVisita);
		visita.setCliente(this.cliente.toEntity());
		visita.setSituacion(this.situacion.toEntity());
		visita.setBidonesCanje(new BigInteger(this.bidonesCanje));
		visita.setBidones(new BigInteger(this.bidones.toString()));
		visita.setFecAlta(new FechaUtil().stringToDateParser(this.fecAlta));
		visita.setUsrAlta(this.usrAlta);
		try {
			visita.setFecUltAct(new FechaUtil().formatoFechaHora(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			visita.setFecUltAct(new FechaUtil().stringToDateParser(this.fecUltAct));
		}
		visita.setUsrUltAct(this.usrUltAct);
		return visita;
	}

	public List<Visita> toEntityList(List<VisitaDTO> listaVisitaDTO) {
		List<Visita> listaVisitaEntity = new ArrayList<Visita>();
		for (VisitaDTO visitaDTO : listaVisitaDTO) {
			listaVisitaEntity.add(visitaDTO.toEntity());
		}
		return listaVisitaEntity;
	}

}
