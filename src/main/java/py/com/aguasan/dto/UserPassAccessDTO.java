package py.com.aguasan.dto;

public class UserPassAccessDTO {

	private String userName;
	private String password;
	private String deviceId;
	private String tokenRegistrationId; //para push notifications

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getTokenRegistrationId() {
		return tokenRegistrationId;
	}

	public void setTokenRegistrationId(String tokenRegistrationId) {
		this.tokenRegistrationId = tokenRegistrationId;
	}

	
	

}
