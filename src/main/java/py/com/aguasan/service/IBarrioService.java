package py.com.aguasan.service;

import py.com.aguasan.dto.BarrioDTO;

public interface IBarrioService {

	public String getBarrios();

	public String getBarrio(String codBarrio, String codCiudad);

	public String createBarrio(BarrioDTO barrioDTO) throws Exception;

	public String updateBarrio(BarrioDTO barrioDTO) throws Exception;

	public void deleteBarrio(String codBarrio, String codCiudad);

}
