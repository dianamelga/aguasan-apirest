package py.com.aguasan.service;

import py.com.aguasan.entity.UsuarioAcceso;

public interface IUsuarioAccesoService {

	public String createUsuarioAcceso(UsuarioAcceso usuarioAcceso);

	public String updateUsuarioAcceso(UsuarioAcceso usuarioAcces);

	public UsuarioAcceso getUsuarioAcceso(String userName);

	public String generateSerialInstall();
}
