package py.com.aguasan.service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import py.com.aguasan.entity.Mail;

public interface IMailService {

	String sendDatabase(Mail mail)  throws AddressException, MessagingException, Exception ;
	String sendMail(Mail mail) throws AddressException, MessagingException, Exception ;
}
