package py.com.aguasan.service;

import java.math.BigInteger;

import py.com.aguasan.dto.UbicacionTipoDTO;

public interface IUbicacionTipoService {

	public String createUbicacionTipo(UbicacionTipoDTO ubicacionTipoDTO);

	public String updateUbicacionTipo(UbicacionTipoDTO ubicacionTipoDTO);

	public String getUbicacionTipo(BigInteger codUbicacionTipo);

	public String getListaUbicacionTipo();

}
