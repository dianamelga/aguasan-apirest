package py.com.aguasan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.TipoDocumentoDAO;
import py.com.aguasan.dto.TipoDocumentoDTO;
import py.com.aguasan.service.ITipoDocumento;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class ITipoDocumentoImpl implements ITipoDocumento {

	@Autowired
	private TipoDocumentoDAO tipoDocumentoDAO;

	Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String createTipoDocumento(TipoDocumentoDTO tipoDocumentoDTO) {
		return gsonBuilder.toJson(tipoDocumentoDAO.createTipoDocumento(tipoDocumentoDTO.toEntity()));

	}

	@Override
	public String updateTipoDocumento(TipoDocumentoDTO tipoDocumentoDTO) {
		return gsonBuilder.toJson(tipoDocumentoDAO.updateTipoDocumento(tipoDocumentoDTO.toEntity()));

	}

	@Override
	public String getTipoDocumento(String codTipoDocumento) {
		return gsonBuilder.toJson(tipoDocumentoDAO.getTipoDocumento(codTipoDocumento));

	}

	@Override
	public String getTipoDocumentos() {
		return gsonBuilder.toJson(tipoDocumentoDAO.getTipoDocumentos());
	}

	@Override
	public String downTipoDocumento(String codTipoDocumento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String upTipoDocumento(String codTipoDocumento) {
		// TODO Auto-generated method stub
		return null;
	}

}
