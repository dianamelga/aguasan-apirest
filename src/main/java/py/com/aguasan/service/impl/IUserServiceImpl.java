package py.com.aguasan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.UsuarioDAO;
import py.com.aguasan.dto.UsuarioDTO;
import py.com.aguasan.entity.Usuario;
import py.com.aguasan.service.IUsuarioService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class IUserServiceImpl implements IUsuarioService {

	@Autowired
	private UsuarioDAO usuarioDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String getAllUsers() throws Exception {
		Usuario usuario = new Usuario();
		return gsonBuilder.toJson(usuario.toDTOList(usuarioDAO.getAllUsers()));
	}

	@Override
	public String getUsuario(String userName, String password) throws Exception {
		return gsonBuilder.toJson(usuarioDAO.access(userName, password));
	}

	@Override
	public String createUsuario(UsuarioDTO usuarioDTO) {
		return gsonBuilder.toJson(usuarioDAO.createUsuario(usuarioDTO.toEntity()).toDTO());
	}

	@Override
	public String downUsuario(UsuarioDTO usuarioDTO) {
		return gsonBuilder.toJson(usuarioDAO.downUsuario(usuarioDTO.toEntity()).toDTO());

	}

	@Override
	public String upUsuario(UsuarioDTO usuarioDTO) {
		return gsonBuilder.toJson(usuarioDAO.downUsuario(usuarioDTO.toEntity()).toDTO());

	}

	@Override
	public String updateUsuario(UsuarioDTO usuarioDTO) {
		return gsonBuilder.toJson(usuarioDAO.updateUsuario(usuarioDTO.toEntity()).toDTO());
	}

}
