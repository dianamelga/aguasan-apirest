package py.com.aguasan.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.FormularioDAO;
import py.com.aguasan.dto.FormularioDTO;
import py.com.aguasan.entity.Formulario;
import py.com.aguasan.report.FormulariosReportRow;
import py.com.aguasan.report.Reporte;
import py.com.aguasan.report.ReporteRow;
import py.com.aguasan.service.IFormularioService;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Service
public class IFormularioServiceImpl implements IFormularioService {

	@Autowired
	private FormularioDAO formularioDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String createFormulario(FormularioDTO formularioDTO) throws Exception {
		return gsonBuilder.toJson(formularioDAO.createFormulario(formularioDTO.toEntity()).toDTO());

	}

	@Override
	public String getFormulario(BigInteger codFormulario) {
		return gsonBuilder.toJson(formularioDAO.getFormulario(codFormulario).toDTO());

	}

	@Override
	public String getFormularioByNroDocumento(String nroDocumento) {
		List<Formulario> formularios = formularioDAO.getFormularioByNroDocumento(nroDocumento);
		if (formularios.size() > 0) {
			return gsonBuilder.toJson(formularios.get(0).toDTOList(formularios));
		} else {
			return gsonBuilder.toJson(formularios);
		}
	}

	@Override
	public String getFormularioByCodCliente(Long codCliente) {
		List<Formulario> formularios = formularioDAO.getFormularioByCodCliente(codCliente);
		if (formularios.size() > 0) {
			return gsonBuilder.toJson(formularios.get(0).toDTOList(formularios));
		} else {
			return gsonBuilder.toJson(formularios);
		}

	}

	@Override
	public String getFormularios() {
		List<Formulario> formularioEntities = formularioDAO.getFormularios();
		List<FormularioDTO> formulariosDTO = new ArrayList<FormularioDTO>();
		for (Formulario formulario : formularioEntities) {
			formulariosDTO.add(formulario.toDTO());
		}
		return gsonBuilder.toJson(formulariosDTO);

	}

	@Override
	public Reporte getFormularioToExport() {
		List<Formulario> listaDeFormularioEntity = formularioDAO.getFormularios();
		List<ReporteRow> rows = new ArrayList<ReporteRow>();
		FormulariosReportRow row = null;
		for (Formulario formulario : listaDeFormularioEntity) {
			row = new FormulariosReportRow();
			row.setNroFormulario(formulario.getNroFormulario());
			row.setCodCliente(formulario.getCodCliente().getCodCliente());
			row.setNombreCliente(formulario.getCodCliente().getNombres());
			row.setApellidoCliente(
					formulario.getCodCliente().getApellidos() != null ? formulario.getCodCliente().getApellidos() : "");
			row.setBidones(formulario.getBidones());
			row.setDispensers(formulario.getDispensers());
			row.setBebederos(formulario.getBebederos());
			row.setUsrAlta(formulario.getUsrAlta());
			row.setUsrUltAct(formulario.getUsrUltAct());
			row.setFecAlta(new FechaUtil().dateToString(formulario.getFecAlta()));
			row.setFecUltAct(new FechaUtil().dateToString(formulario.getFecUltAct()));
			rows.add(row);
		}

		Reporte report = new Reporte("formularios.csv");
		report.setFilePath(report.getFileName());
		report.setRows(rows);

		return report;
	}

}
