package py.com.aguasan.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.firebase.messaging.SendResponse;

import py.com.aguasan.dao.UsuarioAccesoDAO;
import py.com.aguasan.dao.UsuarioDAO;
import py.com.aguasan.entity.AguaSanMessage;
import py.com.aguasan.entity.TokenRegistrationReq;
import py.com.aguasan.entity.Usuario;
import py.com.aguasan.entity.UsuarioAcceso;
import py.com.aguasan.service.IFirebaseAdminService;
@Service
public class IFirebaseAdminServiceImpl implements IFirebaseAdminService {
	@Autowired
	private UsuarioAccesoDAO usuarioAccesoDAO;
	
	@Autowired
	private UsuarioDAO usuarioDAO;
	
	
	@Override
	public void sendPushNotification(AguaSanMessage message)
			throws Exception {

		sendMessage(message, getAllTokensRegistrationId(), true);
		
	}
	
	
	
	@Override
	public void registerToken(TokenRegistrationReq registrationTokenReq) {
		UsuarioAcceso usuarioAcceso = usuarioAccesoDAO.getUsuarioAcceso(registrationTokenReq.getUserName());
		usuarioAcceso.setDeviceId(registrationTokenReq.getDeviceId());
		usuarioAcceso.setTokenRegistrationId(registrationTokenReq.getTokenRegistrationId());
		usuarioAcceso.setFechaUltimaAct(new Date());
		usuarioAccesoDAO.updateUsuarioAcceso(usuarioAcceso);
		
	}
	
	



	@Override
	public String getTokenRegistrationId(String userName) throws Exception {
		UsuarioAcceso usuarioAcceso = usuarioAccesoDAO.getUsuarioAcceso(userName);
		return usuarioAcceso.getTokenRegistrationId();
	}


	

	@Override
	public ArrayList<String> getAllTokensRegistrationId() throws Exception {
		ArrayList<String> registrationTokens = new ArrayList<String>();
		
		List<Usuario> users = usuarioDAO.getAllUsers();
		if(users != null) {
			for(Usuario u : users) {
				UsuarioAcceso usuarioAcceso = usuarioAccesoDAO.getUsuarioAcceso(u.getUserName());
				if(usuarioAcceso != null) {
					String token = usuarioAcceso.getTokenRegistrationId();
					if(token != null && !token.isEmpty())
						registrationTokens.add(token);
				}
			}
		}
		
		return registrationTokens;
	}



	private void sendMessage(AguaSanMessage message, ArrayList<String> registrationTokens, boolean asPushNotification) 
			throws FirebaseMessagingException {
		
		if(asPushNotification) {
			
		}
		
		Notification noti = new Notification(message.getTitle(), message.getMessage());

		MulticastMessage msg = MulticastMessage.builder()
			    .putData("title", message.getTitle())
			    .putData("message", message.getMessage())
			    .putData("notification", "true")
			    .addAllTokens(registrationTokens)
			    .setNotification(noti)
			    .build();
		
			BatchResponse response = FirebaseMessaging.getInstance(FirebaseApp.getInstance("AguaSan")).sendMulticast(msg);
			if (response.getFailureCount() > 0) {
			  List<SendResponse> responses = response.getResponses();
			  List<String> failedTokens = new ArrayList<>();
			  for (int i = 0; i < responses.size(); i++) {
			    if (!responses.get(i).isSuccessful()) {
			      // The order of responses corresponds to the order of the registration tokens.
			      failedTokens.add(registrationTokens.get(i));
			    }	
			  }

			  System.out.println("List of tokens that caused failures: " + failedTokens);
			}
	}

	

}
