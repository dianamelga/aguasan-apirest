package py.com.aguasan.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.SolicitudAccionDAO;
import py.com.aguasan.dto.SolicitudAccionDTO;
import py.com.aguasan.entity.SolicitudAccion;
import py.com.aguasan.service.ISolicitudAccionService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class ISolicitudAccionServiceImpl implements ISolicitudAccionService {

	@Autowired
	private SolicitudAccionDAO solicitudAccionDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	private Gson g = new Gson();

	@Override
	public String createSolicidutAccion(py.com.aguasan.dto.SolicitudAccionDTO solicitudAccionDTO) {
		return gsonBuilder.toJson(solicitudAccionDAO.createSolicidutAccion(solicitudAccionDTO.toEntity()));
	}

	@Override
	public String getListaSolicitudAccion() {
		return gsonBuilder.toJson(this.listaEntitytoDTO(solicitudAccionDAO.getSolicitudesAcciones()));

	}

	// ESTADOS posibles PEND/APRO/RECH
	@Override
	public String updateSolicidutesAcciones(SolicitudAccionDTO solicitudAccionDTO) {
		// solicitudAccionDTO -> viene de afuera
		// Obtener la solicitudAccion en formatoJson
		String solicitudAccionString = getSolicitudAccion(solicitudAccionDTO.getIdAccion().toString());
		// Parseamos el json objecto to Entity
		SolicitudAccion solicitudAccion = g.fromJson(solicitudAccionString, SolicitudAccion.class);
		// solicitudAccion -> viene de la base de datos
		if (solicitudAccionDTO.getEstado().toUpperCase().equals(solicitudAccion.getEstado().toUpperCase())) {
			return null;
		}

		return gsonBuilder.toJson(solicitudAccionDAO.updateSolicidutAccio(solicitudAccionDTO.toEntity()));

	}

	@Override
	public String getSolicitudAccion(String idAccion) {
		SolicitudAccion solicitudAccion = solicitudAccionDAO.getSolicitudAccion(new BigInteger(idAccion));
		return gsonBuilder.toJson(solicitudAccion);
	}

	public List<SolicitudAccion> listaDTOtoEntity(List<SolicitudAccionDTO> listadoDTO) {
		List<SolicitudAccion> listaEntity = new ArrayList<SolicitudAccion>();
		for (SolicitudAccionDTO solicitudAccionDTO : listadoDTO) {
			listaEntity.add(solicitudAccionDTO.toEntity());
		}
		return listaEntity;
	}

	public List<SolicitudAccionDTO> listaEntitytoDTO(List<SolicitudAccion> listaEntity) {
		List<SolicitudAccionDTO> listaDTO = new ArrayList<SolicitudAccionDTO>();
		for (SolicitudAccion solicitudAccion : listaEntity) {
			listaDTO.add(solicitudAccion.toDTO());
		}
		return listaDTO;
	}

}
