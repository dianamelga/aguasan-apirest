package py.com.aguasan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.BarrioDAO;
import py.com.aguasan.dto.BarrioDTO;
import py.com.aguasan.entity.Barrio;
import py.com.aguasan.service.IBarrioService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class IBarrioServiceImpl implements IBarrioService {

	@Autowired
	private BarrioDAO barrioDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String getBarrios() {
		Barrio barrio = new Barrio();
		return gsonBuilder.toJson(barrio.toDTOList(barrioDAO.getBarrios()));

	}

	@Override
	public String getBarrio(String codBarrio, String codCiudad) {
		Barrio barrio = barrioDAO.getBarrio(codBarrio, codCiudad);
		return gsonBuilder.toJson(barrio);
	}

	@Override
	public String createBarrio(BarrioDTO barrioDTO) throws Exception {
		Barrio barrio = barrioDTO.toEntity();
		return gsonBuilder.toJson(barrioDAO.createBarrio(barrio));
	}

	@Override
	public String updateBarrio(BarrioDTO barrioDTO) throws Exception {
		Barrio barrio = barrioDTO.toEntity();
		return gsonBuilder.toJson(barrioDAO.updateBarrio(barrio));
	}

	@Override
	public void deleteBarrio(String codBarrio, String codCiudad) {
		barrioDAO.deleteBarrio(codBarrio, codCiudad);
	}

}
