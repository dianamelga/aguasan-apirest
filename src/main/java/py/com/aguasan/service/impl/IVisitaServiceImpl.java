package py.com.aguasan.service.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.VisitaDAO;
import py.com.aguasan.dto.VisitaDTO;
import py.com.aguasan.entity.Visita;
import py.com.aguasan.report.Reporte;
import py.com.aguasan.report.ReporteRow;
import py.com.aguasan.report.VisitasReportRow;
import py.com.aguasan.service.IVisitaService;

@Service
public class IVisitaServiceImpl implements IVisitaService {

	@Autowired
	private VisitaDAO visitaDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String getVisita(String nroVisita) {
		return gsonBuilder.toJson(visitaDAO.getVisita(new BigInteger(nroVisita)).toDTO());

	}

	@Override
	public String createVisita(VisitaDTO visitaDTO) {
		return gsonBuilder.toJson(visitaDAO.createVisita(visitaDTO.toEntity()).toDTO());

	}

	@Override
	public String updateVisita(VisitaDTO visitaDTO) {
		return gsonBuilder.toJson(visitaDAO.updateVisita(visitaDTO.toEntity()).toDTO());

	}

	@Override
	public String getListaVisita() {
		Visita visita = new Visita();
		return gsonBuilder.toJson(visita.toDTOList(visitaDAO.getListaVisita()));
	}

	@Override
	public String getListaVisitaByCliente(Long codCliente) {
		Visita visita = new Visita();
		return gsonBuilder.toJson(visita.toDTOList(visitaDAO.getListaVisitaByCliente(codCliente)));
	}

	@Override
	public Reporte getVisitasReport() {
		List<Visita> listaVisitaEntitys = visitaDAO.getListaVisita();
		List<ReporteRow> rows = new ArrayList<ReporteRow>();
		VisitasReportRow row = null;
		for (Visita visita : listaVisitaEntitys) {
			row = new VisitasReportRow();
			row.setNroVisita(visita.getNroVisita());
			row.setCodCliente(visita.getCliente().getCodCliente());
			row.setNombre(visita.getCliente().getNombres() + " " + visita.getCliente().getApellidos());
			row.setSituacionVisita(visita.getSituacion().getSituacion());
			row.setBidones(visita.getBidones());

		}
		
		Reporte report = new Reporte("visitas.csv");
		report.setFilePath(report.getFileName());
		report.setRows(rows);
		
		return report;
	}

}
