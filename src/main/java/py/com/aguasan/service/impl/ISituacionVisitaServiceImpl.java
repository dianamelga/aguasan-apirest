package py.com.aguasan.service.impl;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.SituacionVisitaDAO;
import py.com.aguasan.dto.SituacionVisitaDTO;
import py.com.aguasan.entity.SituacionVisita;
import py.com.aguasan.service.ISituacionVisitaService;

@Service
public class ISituacionVisitaServiceImpl implements ISituacionVisitaService {

	@Autowired
	private SituacionVisitaDAO situacionVisitaDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String createSituacionVisita(SituacionVisitaDTO situacionVisitaDTO) {
		return gsonBuilder.toJson(situacionVisitaDAO.createSituacionVisita(situacionVisitaDTO.toEntity()).toDTO());
	}

	@Override
	public String getListaSituacionVisita() {
		SituacionVisita situacionVisita = new SituacionVisita();
		return gsonBuilder.toJson(situacionVisita.toDTOList(situacionVisitaDAO.getListaSituacionVisita()));

	}

	@Override
	public String updateSituacionVisita(SituacionVisitaDTO situacionVisitaDTO) {
		return gsonBuilder.toJson(situacionVisitaDAO.updateSituacionVisita(situacionVisitaDTO.toEntity()).toDTO());

	}

	@Override
	public String getSituacionVisita(String codSituacion) {
		return gsonBuilder.toJson(situacionVisitaDAO.getSituacionVisita(new BigInteger(codSituacion)).toDTO());

	}

}
