package py.com.aguasan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.UsuarioAccesoDAO;
import py.com.aguasan.entity.UsuarioAcceso;
import py.com.aguasan.service.IUsuarioAccesoService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class IUsuarioAccesoServiceImpl implements IUsuarioAccesoService {

	@Autowired
	private UsuarioAccesoDAO usuarioAccesoDAO;

	Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String createUsuarioAcceso(UsuarioAcceso usuarioAcceso) {
		return gsonBuilder.toJson(usuarioAccesoDAO.createUsuarioAcceso(usuarioAcceso));
	}

	@Override
	public String updateUsuarioAcceso(UsuarioAcceso usuarioAcceso) {
		return gsonBuilder.toJson(usuarioAccesoDAO.updateUsuarioAcceso(usuarioAcceso));
	}

	@Override
	public UsuarioAcceso getUsuarioAcceso(String userName) {
		return usuarioAccesoDAO.getUsuarioAcceso(userName);
	}

	@Override
	public String generateSerialInstall() {
		return gsonBuilder.toJson(usuarioAccesoDAO.generateSerialInstall());
	}

}
