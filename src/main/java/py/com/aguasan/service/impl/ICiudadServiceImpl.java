package py.com.aguasan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.CiudadDAO;
import py.com.aguasan.dto.CiudadDTO;
import py.com.aguasan.entity.Ciudad;
import py.com.aguasan.entity.GenericApiResponse;
import py.com.aguasan.service.ICiudadService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class ICiudadServiceImpl implements ICiudadService {

	@Autowired
	private CiudadDAO ciudadDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String getCiudades() {
		Ciudad ciudad = new Ciudad();
		return gsonBuilder.toJson(ciudad.toDTOList(ciudadDAO.getCiudades()));
	}

	@Override
	public String getCiudad(String codCiudad) {
		Ciudad ciudad = ciudadDAO.getCiudad(codCiudad);
		return gsonBuilder.toJson(ciudad.toDTO());
	}

	@Override
	public String createCiudad(CiudadDTO ciudadDTO) throws Exception {
		Ciudad ciudad = ciudadDTO.toEntity();
		return gsonBuilder.toJson(ciudadDAO.createCiudad(ciudad));
	}

	@Override
	public String updateCiudad(CiudadDTO ciudadDTO) throws Exception {
		Ciudad ciudad = ciudadDTO.toEntity();
		return gsonBuilder.toJson(ciudadDAO.updateCiudad(ciudad));
	}

	@Override
	public void deleteCiudad(String codCiudad) {
		ciudadDAO.deleteCiudad(codCiudad);
	}

}
