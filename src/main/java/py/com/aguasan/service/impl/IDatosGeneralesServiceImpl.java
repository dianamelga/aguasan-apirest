package py.com.aguasan.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.BarrioDAO;
import py.com.aguasan.dao.CiudadDAO;
import py.com.aguasan.dao.TipoDocumentoDAO;
import py.com.aguasan.entity.Barrio;
import py.com.aguasan.entity.Ciudad;
import py.com.aguasan.entity.TipoDocumento;
import py.com.aguasan.service.IDatosGeneralesService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class IDatosGeneralesServiceImpl implements IDatosGeneralesService {

	@Autowired
	private BarrioDAO barrioDAO;
	@Autowired
	private CiudadDAO ciudadDAO;
	@Autowired
	private TipoDocumentoDAO tipoDocumentoDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String getBarrios() {
		Barrio barrio = new Barrio();
		return gsonBuilder.toJson(barrio.toDTOList(barrioDAO.getBarrios()));
	}

	@Override
	public String getCiudades() {
		Ciudad ciudad = new Ciudad();
		return gsonBuilder.toJson(ciudad.toDTOList(ciudadDAO.getCiudades()));
	}

	@Override
	public String getTiposDocumentos() {
		TipoDocumento tipoDocumento = new TipoDocumento();
		return gsonBuilder.toJson(tipoDocumento.toDTOList(tipoDocumentoDAO.getTipoDocumentos()));
	}

}
