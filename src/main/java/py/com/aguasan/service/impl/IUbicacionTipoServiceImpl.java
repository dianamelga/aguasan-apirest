package py.com.aguasan.service.impl;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.UbicacionTipoDAO;
import py.com.aguasan.dto.UbicacionTipoDTO;
import py.com.aguasan.service.IUbicacionTipoService;

/**
 * @author Luis Fernando Capdevila Avalos
 * @version 1
 */
@Service
public class IUbicacionTipoServiceImpl implements IUbicacionTipoService {

	@Autowired
	private UbicacionTipoDAO ubicacionesTiposDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String createUbicacionTipo(UbicacionTipoDTO ubicacionTipoDTO) {
		return gsonBuilder.toJson(ubicacionesTiposDAO.createUbicacionTipo(ubicacionTipoDTO.toEntity()));

	}

	@Override
	public String updateUbicacionTipo(UbicacionTipoDTO ubicacionTipoDTO) {
		return gsonBuilder.toJson(ubicacionesTiposDAO.updateUbicacionTipo(ubicacionTipoDTO.toEntity()));

	}

	@Override
	public String getUbicacionTipo(BigInteger codUbicacionTipo) {
		return gsonBuilder.toJson(ubicacionesTiposDAO.getUbicacionTipo(codUbicacionTipo));

	}

	/**
	 * Metodo que no recibe parametro.
	 * 
	 * @return lista de UbicacionTipo en formato JSON,
	 */
	@Override
	public String getListaUbicacionTipo() {
		return gsonBuilder.toJson(ubicacionesTiposDAO.getListaUbicacionTipo());

	}

}
