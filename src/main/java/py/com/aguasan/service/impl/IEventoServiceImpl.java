package py.com.aguasan.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.EventoDAO;
import py.com.aguasan.dto.EventoDTO;
import py.com.aguasan.entity.Evento;
import py.com.aguasan.service.IEventoService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class IEventoServiceImpl implements IEventoService {

	@Autowired
	private EventoDAO eventoDAO;

	private Gson gsonBuilder = new GsonBuilder().create();

	@Override
	public String getEvento(Long nroEvento) {
		Evento evento = eventoDAO.getEvento(nroEvento);
		return gsonBuilder.toJson(evento.toDTO());
	}

	@Override
	public String getEventos() {
		Evento evento = new Evento();
		return gsonBuilder.toJson(evento.toDTOList(eventoDAO.getEventos()));
	}

	@Override
	public String createEvento(EventoDTO eventoDTO) {
		return gsonBuilder.toJson(eventoDAO.createEvento(eventoDTO.toEntity()).toDTO());

	}

	@Override
	public String updateEvento(EventoDTO eventoDTO) {
		return gsonBuilder.toJson(eventoDAO.updateEvento(eventoDTO.toEntity()).toDTO());
	}

	@Override
	public void deleteEvento(Long nroEvento) {
		eventoDAO.deleteEvento(nroEvento);

	}

	@Override
	public String getEventos(String estado) {
		Evento evento = new Evento();
		return gsonBuilder.toJson(evento.toDTOList(eventoDAO.getEventos(estado)));
	}

	@Override
	public String getEventos(String estado, Date fecEvento) {
		Evento evento = new Evento();
		return gsonBuilder.toJson(evento.toDTOList(eventoDAO.getEventos(estado, fecEvento)));
	}

}
