package py.com.aguasan.service.impl;

import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.stereotype.Service;

import py.com.aguasan.entity.Mail;
import py.com.aguasan.service.IMailService;

/**
 * @author Diana Raquel Melgarejo Velgara
 *
 */
@Service
public class IMailServiceImpl implements IMailService {
	Properties mailServerProperties;
	Session getMailSession;
	MimeMessage generateMailMessage;

	@Override
	public String sendDatabase(Mail mail) throws AddressException, MessagingException, Exception {
		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", 587);
		mailServerProperties.put("mail.smtp.auth", true);
		mailServerProperties.put("mail.smtp.starttls.enable", true);

		System.out.println("Mail Server Properties have been setup successfully..");

		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("di.melgarejo@gmail.com"));
		generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("luisfercapde@gmail.com"));
		generateMailMessage.setSubject(mail.getSubject());

		if (mail.getAttachment() != null) {

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			String fileName = mail.getAttachmentName();
			String filePath = "/home/ubuntu/Projects/AguaSan/databases/" + fileName;
			OutputStream file = new FileOutputStream(filePath);
			file.write(hexStringToByteArray(mail.getAttachment()));
			file.close();

			messageBodyPart.attachFile(filePath);
			messageBodyPart.setFileName(mail.getAttachmentName());
			// messageBodyPart.setText(mail.getMessage());
			multipart.addBodyPart(messageBodyPart);

			generateMailMessage.setContent(multipart);
		} else {
			String emailBody = mail.getMessage();
			generateMailMessage.setContent(emailBody, "text/html");
		}

		System.out.println("Mail Session has been created successfully..");

		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");

		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", "dianascodesys@gmail.com", "dianascodesys/2019");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();

		return "Mail sent successfully";
	}

	private byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}

	@Override
	public String sendMail(Mail mail) throws AddressException, MessagingException, Exception {
		// Step1
		System.out.println("\n 1st ===> setup Mail Server Properties..");
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", 587);
		mailServerProperties.put("mail.smtp.auth", true);
		mailServerProperties.put("mail.smtp.starttls.enable", true);

		System.out.println("Mail Server Properties have been setup successfully..");

		// Step2
		System.out.println("\n\n 2nd ===> get Mail Session..");
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		generateMailMessage = new MimeMessage(getMailSession);
		generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(mail.getDestination()));
		generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("di.melgarejo@gmail.com"));
		generateMailMessage.setSubject(mail.getSubject());

		if (mail.getAttachment() != null) {

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			if (!mail.isAttachmentByteArray()) {
				messageBodyPart.attachFile(mail.getAttachment());
			} else {
				String fileName = mail.getAttachmentName();
				String filePath = "/home/ubuntu/Projects/AguaSan/FILES/" + fileName;
				OutputStream file = new FileOutputStream(filePath);
				file.write(hexStringToByteArray(mail.getAttachment()));
				file.close();

				messageBodyPart.attachFile(filePath);
			}
			messageBodyPart.setFileName(mail.getAttachmentName());

			if (mail.getMessage() != null)
				messageBodyPart.setText(mail.getMessage());

			multipart.addBodyPart(messageBodyPart);

			generateMailMessage.setContent(multipart);
		} else {
			String emailBody = mail.getMessage();
			generateMailMessage.setContent(emailBody, "text/html");
		}

		System.out.println("Mail Session has been created successfully..");

		// Step3
		System.out.println("\n\n 3rd ===> Get Session and Send mail");
		Transport transport = getMailSession.getTransport("smtp");

		// Enter your correct gmail UserID and Password
		// if you have 2FA enabled then provide App Specific Password
		transport.connect("smtp.gmail.com", "dianascodesys@gmail.com", "dianascodesys/2019");
		transport.sendMessage(generateMailMessage, generateMailMessage.getAllRecipients());
		transport.close();

		return "Mail sent successfully";
	}

}
