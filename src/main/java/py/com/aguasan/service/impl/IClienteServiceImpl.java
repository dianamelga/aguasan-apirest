package py.com.aguasan.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import py.com.aguasan.dao.ClienteDAO;
import py.com.aguasan.dao.FormularioDAO;
import py.com.aguasan.dao.UbicacionClienteDAO;
import py.com.aguasan.dao.VisitaDAO;
import py.com.aguasan.dto.ClienteDTO;
import py.com.aguasan.dto.FormularioDTO;
import py.com.aguasan.entity.Cliente;
import py.com.aguasan.entity.Formulario;
import py.com.aguasan.entity.UbicacionCliente;
import py.com.aguasan.entity.Visita;
import py.com.aguasan.report.ClienteReportRow;
import py.com.aguasan.report.Reporte;
import py.com.aguasan.report.ReporteRow;
import py.com.aguasan.report.UbicacionesClientesReportRow;
import py.com.aguasan.service.IClienteService;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Service
public class IClienteServiceImpl implements IClienteService {

	@Autowired
	private ClienteDAO clienteDAO;

	@Autowired
	private FormularioDAO formularioDAO;

	@Autowired
	private UbicacionClienteDAO ubicacionClienteDAO;

	@Autowired
	private VisitaDAO visitaDAO;

	public Gson gsonBuilder = new GsonBuilder().create();

	@Value("${img.directory}")
	private String directorioIMG;

	@Transactional
	@Override
	public String createCliente(ClienteDTO clienteDTO) throws Exception {
		ClienteDTO clienteRetorno = clienteDAO.createCliente(clienteDTO.toEntity()).toDTO();
		Formulario formulario = null;
		for (UbicacionCliente ubicacionCliente : clienteDTO.getListUbicacionesClienteDTOtoEntity()) {
			ubicacionCliente.setCliente(clienteDAO.getCliente(clienteRetorno.getCodCliente()));
			ubicacionClienteDAO.createUbicacionCliente(ubicacionCliente);
		}
		FormularioDTO formularioDTO = clienteDTO.getFormulario();
		formularioDTO.setCodCliente(clienteRetorno);
		formularioDTO.setFecAlta(clienteDTO.getFechaAlta());
		formularioDTO.setFecUltAct(clienteDTO.getFechaUltimaAct());
		formularioDTO.setUsrAlta(clienteDTO.getUserAlta());
		formularioDTO.setUsrUltAct(clienteDTO.getUserUltimaAct());
		formulario = formularioDAO.createFormulario(clienteDTO.getFormulario().toEntity());
		clienteRetorno.setFormulario(formulario.toDTO());
		clienteRetorno.setFormularios(formularioDTO.toEntity()
				.toDTOList(formularioDAO.getFormularioByCodCliente(clienteRetorno.getCodCliente())));
		return gsonBuilder.toJson(clienteRetorno);
	}

	@Transactional
	@Override
	public String updateCliente(ClienteDTO clienteDTO) throws Exception {
		ClienteDTO clienteRetorno = clienteDAO.updateCliente(clienteDTO.toEntity()).toDTO();
		boolean haveUbicacionCliente = true;
		Formulario formulario = formularioDAO.getUltimoFormularioByCliente(clienteDTO.getCodCliente());
		if (ubicacionClienteDAO.getUbicacionCliente(clienteDTO.getCodCliente()).isEmpty()) {
			haveUbicacionCliente = false;
		}
		for (UbicacionCliente ubicacionCliente : clienteDTO.getListUbicacionesClienteDTOtoEntity()) {
			ubicacionCliente.setCliente(clienteDAO.getCliente(clienteRetorno.getCodCliente()));
			if (haveUbicacionCliente == false) {
				ubicacionClienteDAO.createUbicacionCliente(ubicacionCliente);
			} else {
				ubicacionClienteDAO.updateUbicacionCliente(ubicacionCliente);
			}
		}
		FormularioDTO formularioDTO = clienteDTO.getFormulario();
		if (formularioDTO != null) {
			formularioDTO.setCodCliente(clienteRetorno);
			formularioDTO.setFecAlta(clienteDTO.getFechaAlta());
			formularioDTO.setFecUltAct(new FechaUtil().dateToString(new Date()));
			formularioDTO.setUsrAlta(clienteDTO.getUserAlta());
			formularioDTO.setUsrUltAct(clienteDTO.getUserUltimaAct());
		}
		if (formulario == null) {
			formularioDAO.createFormulario(formularioDTO.toEntity());
		} else {
			if (formulario.getNroFormulario().compareTo(clienteDTO.getFormulario().getNroFormulario()) == 0) {
				formularioDAO.updateFormulario(formularioDTO.toEntity());
			} else {
				formularioDAO.createFormulario(formularioDTO.toEntity());
			}
		}

		return gsonBuilder.toJson(clienteDTO);

	}

	@Override
	public String getCliente(Long codCliente) {
		ClienteDTO clienteDTO = clienteDAO.getCliente(codCliente).toDTO();
		List<Formulario> formulariosEntities = formularioDAO.getFormularioByCodCliente(codCliente);
		List<FormularioDTO> formulariosDTO = new ArrayList<FormularioDTO>();
		for (Formulario formulario : formulariosEntities) {
			formulariosDTO.add(formulario.toDTO());
		}
		if (formulariosDTO.size() > 0) {
			clienteDTO.setFormulario(formulariosDTO.get(formulariosDTO.size() - 1));
		} else {
			clienteDTO.setFormulario(new FormularioDTO());

		}
		clienteDTO.setListUbicacionesClienteDTO(ubicacionClienteDAO.getUbicacionCliente(codCliente));
		clienteDTO.setVisitas(new Visita().toDTOList(visitaDAO.getListaVisitaByCliente(codCliente)));
		clienteDTO.setFormularios(formulariosDTO);
		return gsonBuilder.toJson(clienteDTO);
	}

	@Override
	public String getClientes(Boolean soloActivos) {
		List<Cliente> clientesEntities = clienteDAO.getClientes(soloActivos);
		List<ClienteDTO> clientesDTO = new ArrayList<ClienteDTO>();
		List<Formulario> formulariosEntities = new ArrayList<Formulario>();
		Formulario formulario = new Formulario();
		for (Cliente cliente : clientesEntities) {
			formulariosEntities = formularioDAO.getFormularioByCodCliente(cliente.getCodCliente());
			ClienteDTO clienteDTO = cliente.toDTO();
			clienteDTO.setListUbicacionesClienteDTO(ubicacionClienteDAO.getUbicacionCliente(cliente.getCodCliente()));
			clienteDTO.setFormularios(formulario.toDTOList(formulariosEntities));
			clienteDTO.setVisitas(new Visita().toDTOList(visitaDAO.getListaVisitaByCliente(cliente.getCodCliente())));
			if (formulariosEntities.size() > 0) {
				clienteDTO.setFormulario(formulariosEntities.get(formulariosEntities.size() - 1).toDTO());
			} else {
				clienteDTO.setFormulario(new FormularioDTO());

			}
			clientesDTO.add(clienteDTO);
		}
		return gsonBuilder.toJson(clientesDTO);
	}

	@Override
	public String downCliente(Long codCliente) {
		return gsonBuilder.toJson(clienteDAO.downCliente(codCliente));
	}

	@Override
	public String upCliente(Long codCliente) {
		return gsonBuilder.toJson(clienteDAO.upCliente(codCliente));
	}

	@Override
	public Reporte getClientesToExport() {
		List<Cliente> clientesEntities = clienteDAO.getClientes(false);
		List<ReporteRow> rows = new ArrayList<ReporteRow>();
		List<Formulario> formulariosEntities = new ArrayList<Formulario>();
		List<UbicacionCliente> listaUbicacionCliente = new ArrayList<UbicacionCliente>();
		String diasDeVisita = null;
		ClienteReportRow row = null;
		for (Cliente cliente : clientesEntities) {
			row = new ClienteReportRow();
			row.setCodCliente(cliente.getCodCliente());
			row.setNombreCliente(cliente.getNombres() + " "+ cliente.getApellidos());
			row.setCelular(cliente.getCelular());
			row.setTelefono(cliente.getTelefono());
			row.setTipoDocumento(cliente.getTipoDocumento().getTipoDocumento());
			row.setNroDocumento(cliente.getNroDocumento());
			row.setCiudad(cliente.getBarrio().getCiudad().getCiudad());
			row.setBarrio(cliente.getBarrio().getBarrio());
			row.setDireccion(cliente.getDireccion());
			row.setObservacion(cliente.getObservacion());
			diasDeVisita = cliente.getDiasVisita().replace("\",\"", " - ").replace("[\"", "").replace("\"]", "");
			row.setDiasVisita(diasDeVisita);
			row.setActivo(cliente.getActivo());
			row.setUserAlta(cliente.getUserAlta());
			row.setFechaAlta(new FechaUtil().dateToString(cliente.getFechaAlta()));
			row.setUserUltimaAct(cliente.getUserUltimaAct());
			row.setFechaUltimaAct(new FechaUtil().dateToString(cliente.getFechaUltimaAct()));
			// obtenemos todos los formularios del cliente
			formulariosEntities = formularioDAO.getFormularioByCodCliente(cliente.getCodCliente());
			// verificacamos para setear el ultimo formulario
			if (formulariosEntities.size() > 0) {
				Formulario form = formulariosEntities.get(formulariosEntities.size() - 1);
				row.setFormulario(form.getNroFormulario().toString());
				row.setBidones(form.getBidones());
				row.setBebederos(form.getBebederos());
				row.setDispensers(form.getDispensers());
			} else {
				row.setFormulario("No tiene formulario");
				row.setBidones(0);
				row.setBebederos(0);
				row.setDispensers(0);

			}
			listaUbicacionCliente = ubicacionClienteDAO.getUbicacionCliente(cliente.getCodCliente());
			if (!listaUbicacionCliente.isEmpty()) {
				row.setLongitud(listaUbicacionCliente.get(0).getLongitud());
				row.setLatitud(listaUbicacionCliente.get(0).getLatitud());
			} else {
				row.setLongitud(" ");
				row.setLatitud(" ");
			}

			rows.add(row);
			row = null;
		} // end for

		Reporte report = new Reporte("clientes.csv");
		report.setFilePath(report.getFileName());
		report.setRows(rows);
		return report;
	}

	@Override
	public int deleteUbicacionCliebnte(Long codUbicacion) {
		return ubicacionClienteDAO.deleteUbicacionCliente(codUbicacion);

	}

	@Transactional
	@Override
	public void deleteCliente(Long codCliente) {
		List<UbicacionCliente> ubicacionesEntities = ubicacionClienteDAO.getUbicacionCliente(codCliente);
		for (UbicacionCliente ubicacion : ubicacionesEntities) {
			ubicacionClienteDAO.deleteUbicacionCliente(ubicacion.getCodUbicacion());
		}
		List<Formulario> formulariosEntities = formularioDAO.getFormularioByCodCliente(codCliente);
		for (Formulario formulario : formulariosEntities) {
			formularioDAO.deleteFormulario(formulario.getNroFormulario());
		}

		clienteDAO.deleteCliente(codCliente);
	}

	@Override
	public Reporte getUbicacionesClientesToExport() {

		List<UbicacionCliente> ubicacionesEntities = ubicacionClienteDAO.getAllUbicacionesClientes();
		List<ReporteRow> rows = new ArrayList<ReporteRow>();

		for (UbicacionCliente ubicacion : ubicacionesEntities) {
			UbicacionesClientesReportRow row = new UbicacionesClientesReportRow(ubicacion.toDTO());
			rows.add(row);
		}

		Reporte report = new Reporte("ubicaciones_clientes.csv");
		report.setFilePath(report.getFileName());
		report.setRows(rows);
		return report;

	}

}
