package py.com.aguasan.service;

import java.math.BigInteger;
import java.util.List;

import py.com.aguasan.dto.FormularioDTO;
import py.com.aguasan.report.FormulariosReportRow;
import py.com.aguasan.report.Reporte;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface IFormularioService {

	public String createFormulario(FormularioDTO formularioDTO) throws Exception;

	public String getFormulario(BigInteger codFormulario);

	public String getFormularioByNroDocumento(String nroDocumento);

	public String getFormularioByCodCliente(Long codCliente);

	public String getFormularios();

	public Reporte getFormularioToExport();
}
