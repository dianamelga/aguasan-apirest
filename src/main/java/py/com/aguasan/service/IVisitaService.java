package py.com.aguasan.service;

import java.util.List;

import py.com.aguasan.dto.VisitaDTO;
import py.com.aguasan.report.Reporte;
import py.com.aguasan.report.VisitasReportRow;

public interface IVisitaService {

	public String getVisita(String nroVisita);

	public String createVisita(VisitaDTO visitaDTO);

	public String updateVisita(VisitaDTO visitaDTO);

	public String getListaVisita();

	public String getListaVisitaByCliente(Long codCliente);

	public Reporte getVisitasReport();

}
