package py.com.aguasan.service;

public interface IDatosGeneralesService {
	public String getBarrios();

	public String getCiudades();

	public String getTiposDocumentos();

}
