package py.com.aguasan.service;

import java.util.Date;
import java.util.List;

import py.com.aguasan.dto.EventoDTO;
import py.com.aguasan.entity.Evento;

public interface IEventoService {

	public String getEvento(Long nroEvento);

	public String getEventos();

	public String getEventos(String estado);
	
	public String getEventos(String estado, Date fecEvento);

	public String createEvento(EventoDTO eventoDTO);

	public String updateEvento(EventoDTO eventoDTO);

	public void deleteEvento(Long nroEvento);
}
