package py.com.aguasan.service;

import py.com.aguasan.dto.SituacionVisitaDTO;

public interface ISituacionVisitaService {

	public String createSituacionVisita(SituacionVisitaDTO situacionVisitaDTO);

	public String getListaSituacionVisita();

	public String updateSituacionVisita(SituacionVisitaDTO situacionVisitaDTO);

	public String getSituacionVisita(String codSituacion);

}
