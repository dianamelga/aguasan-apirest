package py.com.aguasan.service;

import py.com.aguasan.dto.CiudadDTO;
import py.com.aguasan.entity.Ciudad;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface ICiudadService {

	public String getCiudades();

	public String getCiudad(String codCiudad);

	public String createCiudad(CiudadDTO ciudadDTO) throws Exception;

	public String updateCiudad(CiudadDTO ciudadDTO) throws Exception;

	public void deleteCiudad(String codCiudad);
}
