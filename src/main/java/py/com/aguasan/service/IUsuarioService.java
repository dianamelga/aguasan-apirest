package py.com.aguasan.service;

import py.com.aguasan.dto.UsuarioDTO;

public interface IUsuarioService {

	public String getAllUsers() throws Exception;

	public String getUsuario(String userName, String password) throws Exception;

	public String createUsuario(UsuarioDTO usuarioDTO);

	public String updateUsuario(UsuarioDTO usuarioDTO);

	public String downUsuario(UsuarioDTO usuarioDTO);

	public String upUsuario(UsuarioDTO usuarioDTO);

}
