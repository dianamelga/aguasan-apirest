package py.com.aguasan.service;

import py.com.aguasan.dto.TipoDocumentoDTO;

public interface ITipoDocumento {

	public String createTipoDocumento(TipoDocumentoDTO tipoDocumentoDTO);

	public String updateTipoDocumento(TipoDocumentoDTO tipoDocumentoDTO);

	public String getTipoDocumento(String codTipoDocumento);

	public String getTipoDocumentos();

	public String downTipoDocumento(String codTipoDocumento);

	public String upTipoDocumento(String codTipoDocumento);
}
