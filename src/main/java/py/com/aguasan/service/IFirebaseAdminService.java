package py.com.aguasan.service;

import java.util.ArrayList;

import com.google.firebase.messaging.FirebaseMessagingException;

import py.com.aguasan.entity.AguaSanMessage;
import py.com.aguasan.entity.TokenRegistrationReq;

public interface IFirebaseAdminService {

	void sendPushNotification(AguaSanMessage message) throws Exception;
	
	void registerToken(TokenRegistrationReq registrationTokenReq) throws Exception;
	
	String getTokenRegistrationId(String userName) throws Exception;
	
	ArrayList<String> getAllTokensRegistrationId() throws Exception;
	
}
