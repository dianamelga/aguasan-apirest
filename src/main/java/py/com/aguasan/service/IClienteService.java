package py.com.aguasan.service;

import java.util.List;

import py.com.aguasan.dto.ClienteDTO;
import py.com.aguasan.report.ClienteReportRow;
import py.com.aguasan.report.Reporte;

public interface IClienteService {

	public String createCliente(ClienteDTO clienteDTO) throws Exception;

	public String updateCliente(ClienteDTO clienteDTO) throws Exception;

	public String getCliente(Long codCliente);

	public String getClientes(Boolean soloActivos);

	public Reporte getClientesToExport();
	
	public Reporte getUbicacionesClientesToExport();

	public String downCliente(Long codCliente);

	public String upCliente(Long codCliente);

	public int deleteUbicacionCliebnte(Long codUbicacion);

	public void deleteCliente(Long codCliente);

}
