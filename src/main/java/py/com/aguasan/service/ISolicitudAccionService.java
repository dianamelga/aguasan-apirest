package py.com.aguasan.service;

import py.com.aguasan.dto.SolicitudAccionDTO;

public interface ISolicitudAccionService {

	public String createSolicidutAccion(SolicitudAccionDTO solicitudAccionDTO);

	public String getListaSolicitudAccion();

	public String updateSolicidutesAcciones(SolicitudAccionDTO solicitudAccionDTO);

	public String getSolicitudAccion(String idAccion);

}
