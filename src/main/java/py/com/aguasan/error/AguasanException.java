package py.com.aguasan.error;

import javax.persistence.PersistenceException;

/**
 * 
 * @author Luis Fernando Capdevila Avalos
 *
 */
public class AguasanException extends PersistenceException {

	private static final long serialVersionUID = 3412841377955613974L;

	private String causa;

	private String mensaje;

	public String getCausa() {
		return causa;
	}

	public void setCausa(String causa) {
		this.causa = causa;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public AguasanException(String causa, String mensaje) {
		super();
		this.causa = causa;
		this.mensaje = mensaje;
	}

	@Override
	public String toString() {
		return "AguasanException [causa=" + causa + ", mensaje=" + mensaje + "]";
	}

}
