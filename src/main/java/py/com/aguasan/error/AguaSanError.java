package py.com.aguasan.error;

import java.io.Serializable;

/**
 * 
 * @author Diana Raquel Melgarejo Velgara
 *
 */
public class AguaSanError implements Serializable {

	private static final long serialVersionUID = 1L;
	private String code = "";
	private String message = "";
	private boolean useApiMessage = false;

	public AguaSanError() {
	}

	public AguaSanError(String code, String message, boolean useApiMessage) {
		this.code = code;
		this.message = message;
		this.useApiMessage = useApiMessage;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isUseApiMessage() {
		return useApiMessage;
	}

	public void setUseApiMessage(boolean useApiMessage) {
		this.useApiMessage = useApiMessage;
	}

}
