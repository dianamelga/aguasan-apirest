package py.com.aguasan.dao;

import java.util.List;

import py.com.aguasan.entity.TipoDocumento;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface TipoDocumentoDAO {

	public TipoDocumento getTipoDocumento(String codTipoDocumento);

	public List<TipoDocumento> getTipoDocumentos();

	public TipoDocumento createTipoDocumento(TipoDocumento tipoDocumento);

	public TipoDocumento updateTipoDocumento(TipoDocumento tipoDocumento);

	public int downTipoDocumento(String codTipoDocumento);

	public int upTipoDocumento(String codTipoDocumento);

}
