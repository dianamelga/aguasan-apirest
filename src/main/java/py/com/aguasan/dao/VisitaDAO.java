package py.com.aguasan.dao;

import java.math.BigInteger;
import java.util.List;

import py.com.aguasan.entity.Visita;

public interface VisitaDAO {

	public Visita getVisita(BigInteger nroVisita);

	public Visita createVisita(Visita visita);

	public Visita updateVisita(Visita visita);

	public List<Visita> getListaVisita();

	public List<Visita> getListaVisitaByCliente(Long codCliente);

}
