package py.com.aguasan.dao;

import java.util.List;

import py.com.aguasan.entity.Cliente;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface ClienteDAO {

	public Cliente createCliente(Cliente cliente);

	public Cliente updateCliente(Cliente cliente);

	public Cliente getCliente(Long codCliente);

	public Cliente getClienteByNroDocumento(String nroDocumento);

	public List<Cliente> getClientes(Boolean soloActivos);

	public int downCliente(Long codCliente);

	public int upCliente(Long codCliente);

	public void deleteCliente(Long codCliente);

}
