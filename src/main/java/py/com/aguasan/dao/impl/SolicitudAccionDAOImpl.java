package py.com.aguasan.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.SolicitudAccionDAO;
import py.com.aguasan.entity.SolicitudAccion;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class SolicitudAccionDAOImpl implements SolicitudAccionDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public SolicitudAccion getSolicitudAccion(BigInteger idAccion) {
		SolicitudAccion solicitudAccion = entityManager.find(SolicitudAccion.class, idAccion);
		return solicitudAccion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudAccion> getSolicitudesAcciones() {
		String hql = "FROM SolicitudAccion as solicitudAccion ORDER BY solicitudAccion.idAccion";
		List<SolicitudAccion> resultList = (List<SolicitudAccion>) entityManager.createQuery(hql).getResultList();
		return resultList;
	}

	@Override
	public SolicitudAccion createSolicidutAccion(SolicitudAccion solicitudAccion) {
		entityManager.persist(solicitudAccion);
		entityManager.flush();
		return solicitudAccion;
	}

	@Override
	public SolicitudAccion updateSolicidutAccio(SolicitudAccion solicitudAccion) {
		entityManager.merge(solicitudAccion);
		entityManager.flush();
		return solicitudAccion;
	}

	@Override
	public int downSolicidutAccio(String idAccion) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int upSolicidutAccio(String idAccion) {
		// TODO Auto-generated method stub
		return 0;
	}

}
