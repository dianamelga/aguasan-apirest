package py.com.aguasan.dao.impl;

import java.math.BigInteger;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.UsuarioAccesoDAO;
import py.com.aguasan.entity.UsuarioAcceso;

@Repository
@Transactional
public class UsuarioAccesoDAOImpl implements UsuarioAccesoDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public UsuarioAcceso createUsuarioAcceso(UsuarioAcceso usuarioAcceso) {
		entityManager.persist(usuarioAcceso);
		entityManager.flush();
		return usuarioAcceso;
	}

	@Override
	public UsuarioAcceso updateUsuarioAcceso(UsuarioAcceso usuarioAcceso) {
		entityManager.merge(usuarioAcceso);
		entityManager.flush();
		return usuarioAcceso;
	}

	@Override
	public UsuarioAcceso getUsuarioAcceso(String userName) {
		UsuarioAcceso usuarioAcceso = (UsuarioAcceso) entityManager.find(UsuarioAcceso.class, userName);
		return usuarioAcceso;
	}

	@Override
	public BigInteger generateSerialInstall() {
		BigInteger serial = (BigInteger) entityManager.createNativeQuery("SELECT nextval('serial')").getSingleResult();
		return serial;

	}

}
