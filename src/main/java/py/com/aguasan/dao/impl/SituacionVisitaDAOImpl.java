package py.com.aguasan.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.SituacionVisitaDAO;
import py.com.aguasan.entity.SituacionVisita;

@Repository
@Transactional
public class SituacionVisitaDAOImpl implements SituacionVisitaDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public SituacionVisita getSituacionVisita(BigInteger codSituacion) {
		SituacionVisita situacionVisita = entityManager.find(SituacionVisita.class, codSituacion);
		return situacionVisita;
	}

	@Override
	public SituacionVisita createSituacionVisita(SituacionVisita situacionVisita) {
		entityManager.persist(situacionVisita);
		entityManager.flush();
		return situacionVisita;
	}

	@Override
	public SituacionVisita updateSituacionVisita(SituacionVisita situacionVisita) {
		entityManager.merge(situacionVisita);
		entityManager.flush();
		return situacionVisita;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SituacionVisita> getListaSituacionVisita() {
		String hql = "FROM SituacionVisita as situacionVisita ORDER BY situacionVisita.codSituacion";
		List<SituacionVisita> resultList = (List<SituacionVisita>) entityManager.createQuery(hql).getResultList();
		return resultList;
	}

}
