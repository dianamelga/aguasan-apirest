package py.com.aguasan.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.UbicacionClienteDAO;
import py.com.aguasan.entity.UbicacionCliente;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class UbicacionClienteDAOImpl implements UbicacionClienteDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public UbicacionCliente createUbicacionCliente(UbicacionCliente ubicacionCliente) {
		entityManager.persist(ubicacionCliente);
		entityManager.flush();
		return ubicacionCliente;
	}

	@Override
	public UbicacionCliente updateUbicacionCliente(UbicacionCliente ubicacionCliente) {
		entityManager.merge(ubicacionCliente);
		entityManager.flush();
		return ubicacionCliente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UbicacionCliente> getUbicacionCliente(Long codCliente) {
		String hql = "FROM UbicacionCliente as ubiCliente WHERE ubiCliente.cliente.codCliente=:codCliente ORDER BY ubiCliente.fecAlta DESC";
		List<UbicacionCliente> ubicacioneCliente = entityManager.createQuery(hql).setParameter("codCliente", codCliente)
				.getResultList();
		return ubicacioneCliente;
	}

	@Override
	public int deleteUbicacionCliente(Long codUbicacion) {
		String query = "DELETE FROM ubicaciones_clientes where cod_ubicacion = " + codUbicacion;
		int delete = entityManager.createNativeQuery(query).executeUpdate();
		return delete;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UbicacionCliente> getAllUbicacionesClientes() {
		String hql = "FROM UbicacionCliente as ubi ORDER BY ubi.codUbicacion";
		List<UbicacionCliente> ubicacioneCliente = entityManager.createQuery(hql).getResultList();
		return ubicacioneCliente;
	}

}
