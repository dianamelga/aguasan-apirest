package py.com.aguasan.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.UsuarioDAO;
import py.com.aguasan.entity.Usuario;

@Repository
@Transactional
public class UsuarioDAOImpl implements UsuarioDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioDAOImpl.class);

	@Autowired
	private EntityManager entityManager;

	public Usuario access(String userName, String password) {
		Usuario usuario = getUsuario(userName, password);
		return usuario;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getAllUsers() {
		String hql = "FROM Usuario as user ORDER BY user.userName";
		return (List<Usuario>) entityManager.createQuery(hql).getResultList();
	}

	@Override
	public Usuario getUsuario(String userName, String password) {
		Usuario us = null;
		String activo = "S";
		String hql = "FROM Usuario as user WHERE userName=:userName and password=:password and activo=:activo";
		us = (Usuario) entityManager.createQuery(hql).setParameter("userName", userName)
				.setParameter("password", password).setParameter("activo", activo).getSingleResult();
		entityManager.flush();

		return us;

	}

	@Override
	public Usuario createUsuario(Usuario usuario) {
		entityManager.persist(usuario);
		entityManager.flush();
		return usuario;
	}

	@Override
	public Usuario updateUsuario(Usuario usuario) {
		entityManager.merge(usuario);
		entityManager.flush();
		return usuario;
	}

	@Override
	public Usuario downUsuario(Usuario usuario) {
		usuario.setActivo("N");
		entityManager.merge(usuario);
		entityManager.flush();
		return usuario;

	}

	@Override
	public Usuario upUsuario(Usuario usuario) {
		usuario.setActivo("S");
		entityManager.merge(usuario);
		entityManager.flush();
		return usuario;
	}

}
