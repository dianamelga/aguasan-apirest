package py.com.aguasan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.TipoDocumentoDAO;
import py.com.aguasan.entity.TipoDocumento;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class TipoDocumentoDAOImpl implements TipoDocumentoDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public TipoDocumento createTipoDocumento(TipoDocumento tipoDocumento) {
		entityManager.persist(tipoDocumento);
		entityManager.flush();
		return tipoDocumento;
	}

	@Override
	public TipoDocumento updateTipoDocumento(TipoDocumento tipoDocumento) {
		entityManager.merge(tipoDocumento);
		entityManager.flush();
		return tipoDocumento;
	}

	@Override
	public TipoDocumento getTipoDocumento(String codTipoDocumento) {
		String hql = "FROM TipoDocumento WHERE codTipoDocumentos=:codTipoDocumento ";
		TipoDocumento tipoDocumento = (TipoDocumento) entityManager.createQuery(hql)
				.setParameter("codTipoDocumento", codTipoDocumento).getSingleResult();
		return tipoDocumento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TipoDocumento> getTipoDocumentos() {
		String hql = "FROM TipoDocumento as tipoDocumento ORDER BY tipoDocumento.codTipoDocumento";
		List<TipoDocumento> resultList = (ArrayList<TipoDocumento>) entityManager.createQuery(hql).getResultList();
		return resultList;
	}

	@Override
	public int downTipoDocumento(String codTipoDocumento) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int upTipoDocumento(String codTipoDocumento) {
		// TODO Auto-generated method stub
		return 0;
	}

}
