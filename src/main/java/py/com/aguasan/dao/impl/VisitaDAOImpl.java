package py.com.aguasan.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.VisitaDAO;
import py.com.aguasan.entity.Visita;

@Repository
@Transactional
public class VisitaDAOImpl implements VisitaDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public Visita getVisita(BigInteger nroVisita) {
		Visita visita = entityManager.find(Visita.class, nroVisita);
		return visita;
	}

	@Override
	public Visita createVisita(Visita visita) {
		entityManager.persist(visita);
		entityManager.flush();
		return visita;
	}

	@Override
	public Visita updateVisita(Visita visita) {
		entityManager.merge(visita);
		entityManager.flush();
		return visita;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Visita> getListaVisita() {
		String hql = "FROM Visita as visita ORDER BY visita.nroVisita";
		List<Visita> resultList = (List<Visita>) entityManager.createQuery(hql).getResultList();
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Visita> getListaVisitaByCliente(Long codCliente) {
		String hql = "FROM Visita as visita where visita.cliente.codCliente=:codCliente ORDER BY visita.nroVisita DESC";
		List<Visita> resultList = (List<Visita>) entityManager.createQuery(hql).setParameter("codCliente", codCliente)
				.getResultList();
		return resultList;
	}

}
