package py.com.aguasan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.CiudadDAO;
import py.com.aguasan.entity.Barrio;
import py.com.aguasan.entity.Ciudad;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class CiudadDAOImpl implements CiudadDAO {

	@Autowired
	EntityManager entityManager;

	@Override
	public Ciudad createCiudad(Ciudad ciudad) {
		entityManager.persist(ciudad);
		entityManager.flush();
		return ciudad;
	}

	@Override
	public Ciudad updateCiudad(Ciudad ciudad) {
		entityManager.merge(ciudad);
		entityManager.flush();
		return ciudad;
	}

	@Override
	public Ciudad getCiudad(String codCiudad) {
		String hql = "FROM Ciudad as ciudad WHERE ciudad.codCiudad=:codCiudad ";
		Ciudad ciudad = (Ciudad) entityManager.createQuery(hql).setParameter("codCiudad", codCiudad).getSingleResult();
		return ciudad;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Ciudad> getCiudades() {
		String hql = "FROM Ciudad as ciudad ORDER BY ciudad.ciudad";
		List<Ciudad> resultList = (ArrayList<Ciudad>) entityManager.createQuery(hql).getResultList();
		entityManager.flush();
		return resultList;
	}

	@Override
	public int downCiudad(String codCiudad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int upCiudad(String codCiudad) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteCiudad(String codCiudad) {
		Ciudad ciudad = this.getCiudad(codCiudad);
		entityManager.remove(ciudad);
		entityManager.flush();
		
	}

}
