package py.com.aguasan.dao.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.EventoDAO;
import py.com.aguasan.entity.Barrio;
import py.com.aguasan.entity.Evento;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class EventoDAOImpl implements EventoDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public Evento getEvento(Long nroEvento) {
		String hql = "FROM Evento e WHERE e.nroEvento=:nroEvento ";
		Evento evento = (Evento) entityManager.createQuery(hql).setParameter("nroEvento", nroEvento).getSingleResult();
		evento = entityManager.find(Evento.class, nroEvento);
		return evento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> getEventos() {
		String hql = "FROM Evento f order by f.nroEvento DESC";
		List<Evento> eventos = (List<Evento>) entityManager.createQuery(hql).getResultList();
		return eventos;
	}

	@Override
	public Evento createEvento(Evento evento) {
		entityManager.persist(evento);
		entityManager.flush();
		return evento;
	}

	@Override
	public Evento updateEvento(Evento evento) {
		entityManager.merge(evento);
		entityManager.flush();
		return evento;
	}

	@Override
	public void deleteEvento(Long nroEvento) {
		entityManager.detach(this.getEvento(nroEvento));
		entityManager.flush();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> getEventos(String estado) {
		String hql = "FROM Evento f WHERE f.estado = :estado order by f.nroEvento DESC";
		List<Evento> eventos = (ArrayList<Evento>) entityManager.createQuery(hql).setParameter("estado", estado).getResultList();
		return eventos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Evento> getEventos(String estado, Date fecEvento) {
		 
			Calendar c = Calendar.getInstance();
			c.setTime(fecEvento);
		
			//Incrementing the date by 1 day
			c.add(Calendar.DAY_OF_MONTH, 1);
			
			Date fecEventoHasta = c.getTime();
		
		
		String hql = "FROM Evento f WHERE f.estado = :estado AND f.fecEvento >= :fecEvento AND f.fecEvento < :fecEventoHasta order by f.nroEvento DESC";
		List<Evento> eventos = (ArrayList<Evento>) entityManager.createQuery(hql).setParameter("estado", estado)
				.setParameter("fecEvento", fecEvento)
				.setParameter("fecEventoHasta", fecEventoHasta).getResultList();
		return eventos;
	}

}
