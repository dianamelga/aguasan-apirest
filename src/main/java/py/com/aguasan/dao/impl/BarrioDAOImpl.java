package py.com.aguasan.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.BarrioDAO;
import py.com.aguasan.entity.Barrio;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class BarrioDAOImpl implements BarrioDAO {

	@Autowired
	EntityManager entityManager;

	@Override
	public Barrio createBarrio(Barrio barrio) {
		entityManager.persist(barrio);
		entityManager.flush();
		return barrio;
	}

	@Override
	public Barrio updateBarrio(Barrio barrio) {
		entityManager.merge(barrio);
		entityManager.flush();
		return barrio;
	}

	@Override
	public Barrio getBarrio(String codBarrio, String codCiudad) {
		String hql = "FROM Barrio as barrio WHERE barrio.codBarrio=:codBarrio AND barrio.ciudad.codCiudad =:codCiudad ";
		Barrio barrio = (Barrio) entityManager.createQuery(hql).setParameter("codBarrio", codBarrio)
				.setParameter("codCiudad",codCiudad).getSingleResult();
		return barrio;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Barrio> getBarrios() {
		String hql = "FROM Barrio as barrio ORDER BY barrio.barrio";
		List<Barrio> resultList = (ArrayList<Barrio>) entityManager.createQuery(hql).getResultList();
		entityManager.flush();
		return resultList;
	}

	@Override
	public void deleteBarrio(String codBarrio, String codCiudad) {
		Barrio barrio = this.getBarrio(codBarrio, codCiudad);
		entityManager.remove(barrio);
		entityManager.flush();

	}

	@Override
	public int upBarrio(String codBarrio) {
		// TODO Auto-generated method stub
		return 0;
	}

}
