package py.com.aguasan.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.UbicacionTipoDAO;
import py.com.aguasan.entity.UbicacionTipo;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Repository
@Transactional
public class UbicacionTipoDAOImpl implements UbicacionTipoDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public UbicacionTipo createUbicacionTipo(UbicacionTipo ubicacionTipo) {
		entityManager.persist(ubicacionTipo);
		entityManager.flush();
		return ubicacionTipo;
	}

	@Override
	public UbicacionTipo updateUbicacionTipo(UbicacionTipo ubicacionTipo) {
		entityManager.merge(ubicacionTipo);
		entityManager.flush();
		return ubicacionTipo;
	}

	@Override
	public UbicacionTipo getUbicacionTipo(BigInteger codUbicacionTipo) {
		return entityManager.find(UbicacionTipo.class, codUbicacionTipo);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UbicacionTipo> getListaUbicacionTipo() {
		String hql = "FROM UbicacionTipo as ubicacionTipo ORDER BY ubicacionTipo.codTipoUbicacion";
		return (List<UbicacionTipo>) entityManager.createQuery(hql).getResultList();

	}

	@Override
	public int downUbicacionTipo(BigInteger codUbicacionTipo) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int upUbicacionTipo(BigInteger codUbicacionTipo) {
		// TODO Auto-generated method stub
		return 0;
	}

}
