package py.com.aguasan.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.ClienteDAO;
import py.com.aguasan.entity.Cliente;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class ClienteDAOImpl implements ClienteDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public Cliente createCliente(Cliente cliente) {
		entityManager.persist(cliente);
		entityManager.flush();
		return cliente;
	}

	@Override
	public Cliente updateCliente(Cliente cliente) {
		entityManager.merge(cliente);
		entityManager.flush();
		return cliente;
	}

	@Override
	public Cliente getCliente(Long codCliente) {
		String hql = "FROM Cliente as cliente  WHERE cliente.codCliente=:codCliente ";
		Cliente cliente = (Cliente) entityManager.createQuery(hql).setParameter("codCliente", codCliente)
				.getSingleResult();
		return cliente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cliente> getClientes(Boolean soloActivos) {
		String hql = "";
		if(soloActivos) {
			hql = "FROM Cliente as cliente WHERE cliente.activo = 'S' ORDER BY cliente.codCliente desc";
		}else {
			hql = "FROM Cliente as cliente ORDER BY cliente.codCliente desc";
		}
		List<Cliente> list = (List<Cliente>) entityManager.createQuery(hql).getResultList();
		return list;
	}

	@Override
	public int downCliente(Long codCliente) {
		String hql = "update FROM Cliente as cliente set cliente.activo = 'N' WHERE cliente.codCliente=:codCliente ";
		int update = entityManager.createQuery(hql).setParameter("codCliente", codCliente).executeUpdate();
		return update;
	}

	@Override
	public int upCliente(Long codCliente) {
		String hql = "update FROM Cliente as cliente set cliente.activo = 'S' WHERE cliente.codCliente=:codCliente ";
		int update = entityManager.createQuery(hql).setParameter("codCliente", codCliente).executeUpdate();
		return update;
	}

	@Override
	public void deleteCliente(Long codCliente) {
		String query = "DELETE FROM clientes where cod_cliente = " + codCliente;
		entityManager.createNativeQuery(query).executeUpdate();
	}

	@Override
	public Cliente getClienteByNroDocumento(String nroDocumento) {
		String hql = "FROM Cliente as cliente  WHERE cliente.nroDocumento=:nroDocumento ";
		Cliente cliente = (Cliente) entityManager.createQuery(hql).setParameter("nroDocumento", nroDocumento)
				.getSingleResult();
		return cliente;
	}

}
