package py.com.aguasan.dao.impl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import py.com.aguasan.dao.FormularioDAO;
import py.com.aguasan.entity.Formulario;
import py.com.aguasan.error.AguasanException;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Repository
@Transactional
public class FormularioDAOImpl implements FormularioDAO {

	@Autowired
	private EntityManager entityManager;

	@Override
	public Formulario createFormulario(Formulario formulario) throws Exception {
		try {
			entityManager.persist(formulario);
			entityManager.flush();
		} catch (PersistenceException e) {
			e.printStackTrace();
			throw new AguasanException("Duplikey Formulario ", "El Formulario ya existe para otro Cliente");
		}
		return formulario;
	}

	@Override
	public Formulario updateFormulario(Formulario formulario) {
		entityManager.merge(formulario);
		entityManager.flush();
		return formulario;
	}

	@Override
	public Formulario getFormulario(BigInteger nroFormulario) {
		String hql = "FROM Formulario f WHERE f.nroFormulario=:nroFormulario ";
		Formulario formulario = (Formulario) entityManager.createQuery(hql).setParameter("nroFormulario", nroFormulario)
				.getSingleResult();
		return formulario;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Formulario> getFormularioByNroDocumento(String nroDocumento) {
		String hql = "FROM Formulario f WHERE f.codCliente.nroDocumento=:nroDocumento order by f.nroFormulario ";
		List<Formulario> formulariosByNroDocumento = (List<Formulario>) entityManager.createQuery(hql)
				.setParameter("nroDocumento", nroDocumento).getResultList();
		return formulariosByNroDocumento;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Formulario> getFormularioByCodCliente(Long codCliente) {
		String hql = "FROM Formulario f WHERE f.codCliente.codCliente=:codCliente order by f.nroFormulario";
		List<Formulario> formulariosByNroCodCliente = (List<Formulario>) entityManager.createQuery(hql)
				.setParameter("codCliente", codCliente).getResultList();
		return formulariosByNroCodCliente;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Formulario getUltimoFormularioByCliente(Long codCliente) {
		String hql = "FROM Formulario f WHERE f.codCliente.codCliente=:codCliente order by f.nroFormulario desc";
		List<Formulario> formulariosByNroCodCliente = (List<Formulario>) entityManager.createQuery(hql)
				.setParameter("codCliente", codCliente).getResultList();

		if (formulariosByNroCodCliente.size() > 0) {
			return formulariosByNroCodCliente.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Formulario> getFormularios() {
		String hql = "FROM Formulario f order by f.nroFormulario ";
		List<Formulario> formulariosByNroCodCliente = (List<Formulario>) entityManager.createQuery(hql).getResultList();
		return formulariosByNroCodCliente;
	}

	@Override
	public void deleteFormulario(BigInteger nroFormulario) {
		String query = "DELETE FROM formulario where nro_formulario = " + nroFormulario;
		entityManager.createNativeQuery(query).executeUpdate();
	}

}
