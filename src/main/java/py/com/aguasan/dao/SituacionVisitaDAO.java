package py.com.aguasan.dao;

import java.math.BigInteger;
import java.util.List;

import py.com.aguasan.entity.SituacionVisita;

public interface SituacionVisitaDAO {

	public SituacionVisita getSituacionVisita(BigInteger codSituacion);

	public SituacionVisita createSituacionVisita(SituacionVisita situacionVisita);

	public SituacionVisita updateSituacionVisita(SituacionVisita situacionVisita);

	public List<SituacionVisita> getListaSituacionVisita();
}
