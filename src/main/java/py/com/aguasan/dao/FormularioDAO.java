package py.com.aguasan.dao;

import java.math.BigInteger;
import java.util.List;

import py.com.aguasan.entity.Formulario;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface FormularioDAO {

	public Formulario createFormulario(Formulario formulario) throws Exception;

	public Formulario updateFormulario(Formulario formulario);

	public Formulario getFormulario(BigInteger nroFormulario);

	public List<Formulario> getFormularioByNroDocumento(String nroDocumento);

	public List<Formulario> getFormularioByCodCliente(Long codCliente);

	public List<Formulario> getFormularios();

	public Formulario getUltimoFormularioByCliente(Long codCliente);
	
	public void deleteFormulario(BigInteger nroFormulario);

}
