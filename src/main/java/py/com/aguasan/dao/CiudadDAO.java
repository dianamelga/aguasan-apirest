package py.com.aguasan.dao;

import java.util.List;

import py.com.aguasan.entity.Ciudad;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface CiudadDAO {

	public Ciudad getCiudad(String codCiudad);

	public List<Ciudad> getCiudades();

	public Ciudad createCiudad(Ciudad ciudad);

	public Ciudad updateCiudad(Ciudad ciudad);
	
	public void deleteCiudad(String codCiudad);

	public int downCiudad(String codCiudad);

	public int upCiudad(String codCiudad);
}
