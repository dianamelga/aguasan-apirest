package py.com.aguasan.dao;

import java.math.BigInteger;
import java.util.List;

import py.com.aguasan.entity.UbicacionTipo;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface UbicacionTipoDAO {

	public UbicacionTipo createUbicacionTipo(UbicacionTipo ubicacionTipo);

	public UbicacionTipo updateUbicacionTipo(UbicacionTipo ubicacionTipo);

	public UbicacionTipo getUbicacionTipo(BigInteger codUbicacionTipo);

	public List<UbicacionTipo> getListaUbicacionTipo();

	public int downUbicacionTipo(BigInteger codUbicacionTipo);

	public int upUbicacionTipo(BigInteger codUbicacionTipo);
}
