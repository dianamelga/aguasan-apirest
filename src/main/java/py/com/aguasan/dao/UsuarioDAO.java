package py.com.aguasan.dao;

import java.util.List;

import py.com.aguasan.entity.Usuario;

public interface UsuarioDAO {

	public Usuario access(String userName, String password);

	public List<Usuario> getAllUsers();

	public Usuario getUsuario(String userName, String password);

	public Usuario createUsuario(Usuario usuario);

	public Usuario updateUsuario(Usuario usuario);

	public Usuario downUsuario(Usuario usuario);

	public Usuario upUsuario(Usuario usuario);

}
