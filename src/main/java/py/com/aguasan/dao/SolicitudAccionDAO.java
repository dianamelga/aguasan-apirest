package py.com.aguasan.dao;

import java.math.BigInteger;
import java.util.List;

import py.com.aguasan.entity.SolicitudAccion;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface SolicitudAccionDAO {

	public SolicitudAccion getSolicitudAccion(BigInteger idAccion);

	public SolicitudAccion createSolicidutAccion(SolicitudAccion solicitudAccion);

	public SolicitudAccion updateSolicidutAccio(SolicitudAccion solicitudAccion);

	public List<SolicitudAccion> getSolicitudesAcciones();

	public int downSolicidutAccio(String idAccion);

	public int upSolicidutAccio(String idAccion);

}
