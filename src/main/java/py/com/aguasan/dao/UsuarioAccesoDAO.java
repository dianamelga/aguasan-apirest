package py.com.aguasan.dao;

import java.math.BigInteger;

import py.com.aguasan.entity.UsuarioAcceso;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface UsuarioAccesoDAO {

	public UsuarioAcceso createUsuarioAcceso(UsuarioAcceso usuarioAcceso);

	public UsuarioAcceso updateUsuarioAcceso(UsuarioAcceso usuarioAcceso);

	public UsuarioAcceso getUsuarioAcceso(String userName);

	public BigInteger generateSerialInstall();

	/**
	 * aqui no existen estados
	 */

}
