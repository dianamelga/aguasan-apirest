package py.com.aguasan.dao;

import java.util.Date;
import java.util.List;

import py.com.aguasan.entity.Evento;

/**
 * 
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface EventoDAO {

	public Evento getEvento(Long nroEvento);

	public List<Evento> getEventos();
	
	public List<Evento> getEventos(String estado);
	
	public List<Evento> getEventos(String estado, Date fecEvento);

	public Evento createEvento(Evento evento);

	public Evento updateEvento(Evento evento);

	public void deleteEvento(Long nroEvento);

}
