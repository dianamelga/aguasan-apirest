package py.com.aguasan.dao;

import java.util.List;

import py.com.aguasan.entity.UbicacionCliente;

public interface UbicacionClienteDAO {

	public UbicacionCliente createUbicacionCliente(UbicacionCliente ubicacionCliente);

	public UbicacionCliente updateUbicacionCliente(UbicacionCliente ubicacionCliente);

	public List<UbicacionCliente> getUbicacionCliente(Long cliente);

	public int deleteUbicacionCliente(Long codUbicacion);
	
	public List<UbicacionCliente> getAllUbicacionesClientes();

}
