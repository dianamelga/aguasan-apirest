package py.com.aguasan.dao;

import java.util.List;

import py.com.aguasan.entity.Barrio;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface BarrioDAO {

	public Barrio getBarrio(String codBarrio, String codCiudad);

	public List<Barrio> getBarrios();

	public Barrio createBarrio(Barrio barrio);

	public Barrio updateBarrio(Barrio barrio);

	public void deleteBarrio(String codBarrio, String codCiudad);

	public int upBarrio(String codBarrio);
}
