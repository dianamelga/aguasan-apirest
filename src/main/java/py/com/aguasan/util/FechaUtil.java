package py.com.aguasan.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Repository;

@Repository
public class FechaUtil {
	private final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	public Date formatoFechaHora(Date fechaHora) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		String dateString = format.format(fechaHora);
		Date date = null;
		try {
			date = format.parse(dateString);
		} catch (Exception e) {
			e.printStackTrace();

		}
		return date;
	}

	public Date stringToDateParser(String dateString) {
		DateFormat format = new SimpleDateFormat(DATE_FORMAT);
		Date date = null;
		try {
			date = format.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String dateStr = dateToString(date);
		return date;
	}

	public String dateToString(Date date) {
		SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
		return format.format(date.getTime());
	}
	
	public Date getDateWithoutTimeUsingCalendar() {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	 
	    return calendar.getTime();
	}

}
