package py.com.aguasan.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.opencsv.CSVReader;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import py.com.aguasan.report.ClienteReportRow;
import py.com.aguasan.report.FormulariosReportRow;
import py.com.aguasan.report.Reporte;
import py.com.aguasan.report.ReporteRow;
import py.com.aguasan.report.UbicacionesClientesReportRow;
import py.com.aguasan.report.VisitasReportRow;

/**
 * 
 * @author Luis Fernando Capdevila Avalos
 *
 */
public class ManejoDeArchivos {

	// @Value("${file.directory}")
	private String pathFileDirectory = "../FILES/";

	@Value("${file.directory}")
	private String pathFileDirectory2;

	/**
	 * empaquetaren en un zip
	 * 
	 * @param srcFiles
	 * @return
	 * @throws IOException
	 */
	public String toZip(List<String> srcFiles) throws IOException {

		String pathFile = pathFileDirectory + "reportes.zip";
		FileOutputStream fos = new FileOutputStream(pathFile);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		for (String srcFile : srcFiles) {
			File fileToZip = new File(srcFile);
			FileInputStream fis = new FileInputStream(fileToZip);
			ZipEntry zipEntry = new ZipEntry(fileToZip.getName());
			zipOut.putNextEntry(zipEntry);

			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zipOut.write(bytes, 0, length);
			}
			fis.close();
		}
		zipOut.close();
		fos.close();
		return pathFile;

	}

	/**
	 * Convertir un Objeto en CSV
	 * 
	 * @param <T>
	 * 
	 * @param listaClienteReporteDTO
	 * @throws Exception
	 */
	public <T> String toCSV(Reporte reporte) throws Exception {

		String fileName = reporte.getFileName();

		String filePath = pathFileDirectory + fileName;
		OutputStream os = new FileOutputStream(pathFileDirectory + fileName);
		System.out.println("Iniciando....");
		System.out.println("Escribiendo archivo CSV....");
		Writer writer = null;
//		PrintWriter writer = null;
		reporte.setFilePath(filePath);

//		writer = new FileWriter(filePath);
		writer = new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8);
//		writer = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));

		if (reporte.getRows().size() > 0) {
			ReporteRow row = reporte.getRows().get(0);
			if (row instanceof ClienteReportRow) {
				CustomMappingStrategy<ClienteReportRow> mappingStrategy = new CustomMappingStrategy<ClienteReportRow>();
				mappingStrategy.setType(ClienteReportRow.class);

				StatefulBeanToCsvBuilder<ClienteReportRow> builder = new StatefulBeanToCsvBuilder<ClienteReportRow>(
						writer);

				StatefulBeanToCsv<ClienteReportRow> beanWriter = builder.withMappingStrategy(mappingStrategy).build();

				for (ReporteRow reporteRow : reporte.getRows()) {
					beanWriter.write((ClienteReportRow) reporteRow);
				}

			} else if (row instanceof FormulariosReportRow) {

				CustomMappingStrategy<FormulariosReportRow> mappingStrategy = new CustomMappingStrategy<FormulariosReportRow>();
				mappingStrategy.setType(FormulariosReportRow.class);

				StatefulBeanToCsvBuilder<FormulariosReportRow> builder = new StatefulBeanToCsvBuilder<FormulariosReportRow>(
						writer);

				StatefulBeanToCsv<FormulariosReportRow> beanWriter = builder.withMappingStrategy(mappingStrategy)
						.build();

				for (ReporteRow reporteRow : reporte.getRows()) {
					beanWriter.write((FormulariosReportRow) reporteRow);
				}

			} else if (row instanceof UbicacionesClientesReportRow) {

				CustomMappingStrategy<UbicacionesClientesReportRow> mappingStrategy = new CustomMappingStrategy<UbicacionesClientesReportRow>();
				mappingStrategy.setType(UbicacionesClientesReportRow.class);

				StatefulBeanToCsvBuilder<UbicacionesClientesReportRow> builder = new StatefulBeanToCsvBuilder<UbicacionesClientesReportRow>(
						writer);

				StatefulBeanToCsv<UbicacionesClientesReportRow> beanWriter = builder
						.withMappingStrategy(mappingStrategy).build();

				for (ReporteRow reporteRow : reporte.getRows()) {
					beanWriter.write((UbicacionesClientesReportRow) reporteRow);
				}

			} else if (row instanceof VisitasReportRow) {

				CustomMappingStrategy<VisitasReportRow> mappingStrategy = new CustomMappingStrategy<VisitasReportRow>();
				mappingStrategy.setType(VisitasReportRow.class);

				StatefulBeanToCsvBuilder<VisitasReportRow> builder = new StatefulBeanToCsvBuilder<VisitasReportRow>(
						writer);

				StatefulBeanToCsv<VisitasReportRow> beanWriter = builder.withMappingStrategy(mappingStrategy).build();

				for (ReporteRow reporteRow : reporte.getRows()) {
					beanWriter.write((VisitasReportRow) reporteRow);
				}

			} else {
				writer.close();
				throw new Exception("Uknown ReporteRow Class: " + row.getClass().getName());
			}
		}
		writer.close();
		System.out.println("Fin del proceso.");
		return filePath;

	}

	/**
	 * Convertir un Fil en Byte
	 * 
	 * @param file
	 * @return
	 */
	private byte[] readFileToByteArray(File file) {
		FileInputStream fis = null;
		byte[] bArray = new byte[(int) file.length()];
		try {
			fis = new FileInputStream(file);
			fis.read(bArray);
			fis.close();

		} catch (IOException ioExp) {
			ioExp.printStackTrace();
		}
		return bArray;
	}

	/**
	 * Convertir un File en un tipo de objecto
	 * 
	 * @param <T>
	 * @param type
	 * @param fileName
	 * @return
	 */
	public <T> List<T> loadObjectList(Class<T> type, String fileName) {
		try {
			CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
			CsvMapper mapper = new CsvMapper();
			File file = new ClassPathResource(fileName).getFile();
			MappingIterator<T> readValues = mapper.reader(type).with(bootstrapSchema).readValues(file);
			return readValues.readAll();
		} catch (Exception e) {
			return Collections.emptyList();
		}
	}

	/**
	 * Convertir un CVS en lista de lista de Strings
	 * 
	 * @param filePath
	 */
	public void cvsToListStrings(String filePath) {
		List<List<String>> records = new ArrayList<List<String>>();
		try (CSVReader csvReader = new CSVReader(new FileReader(filePath));) {
			String[] values = null;
			while ((values = csvReader.readNext()) != null) {
				records.add(Arrays.asList(values));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
