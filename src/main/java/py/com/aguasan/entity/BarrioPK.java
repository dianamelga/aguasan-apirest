package py.com.aguasan.entity;

import java.io.Serializable;

public class BarrioPK implements Serializable {
    protected String codBarrio;
    protected Ciudad ciudad;
	public String getCodBarrio() {
		return codBarrio;
	}
	public void setCodBarrio(String codBarrio) {
		this.codBarrio = codBarrio;
	}
	public Ciudad getCiudad() {
		return ciudad;
	}
	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
        int result = 1;
        result = prime * result + ((codBarrio == null) ? 0 : codBarrio.hashCode());
        result = prime * result + ((ciudad == null) ? 0 : ciudad.hashCode());
        return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BarrioPK other = (BarrioPK) obj;
        if (codBarrio == null) {
            if (other.codBarrio != null)
                return false;
        } else if (!ciudad.getCodCiudad().equals(other.ciudad.getCodCiudad()))
            return false;
        if (codBarrio == null) {
            if (other.codBarrio != null)
                return false;
        } else if (!codBarrio.equals(other.codBarrio))
            return false;
        return true;
	}



    
}
