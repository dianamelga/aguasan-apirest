package py.com.aguasan.entity;

import java.io.Serializable;

public class ReportesReq implements Serializable {

	private String mailDestino;

	public String getMailDestino() {
		return mailDestino;
	}

	public void setMailDestino(String mailDestino) {
		this.mailDestino = mailDestino;
	}
	
	
}
