package py.com.aguasan.entity;

import java.io.Serializable;

public class GenericApiResponse implements Serializable{

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
