package py.com.aguasan.entity;

import java.io.Serializable;

public class Mail implements Serializable {

	private String destination;
	private String subject;
	private String message;
	private String attachment;
	private String attachmentName;
	private boolean isAttachmentByteArray = false;

	public boolean isAttachmentByteArray() {
		return isAttachmentByteArray;
	}

	public void setAttachmentByteArray(boolean isAttachmentByteArray) {
		this.isAttachmentByteArray = isAttachmentByteArray;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

}
