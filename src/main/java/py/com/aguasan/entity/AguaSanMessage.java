package py.com.aguasan.entity;

import java.util.ArrayList;

public class AguaSanMessage {
	private String title;
	private String message;
	private ArrayList<String> users;

	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ArrayList<String> getUsers() {
		return users;
	}
	public void setUsers(ArrayList<String> users) {
		this.users = users;
	}
	

	
	

}
