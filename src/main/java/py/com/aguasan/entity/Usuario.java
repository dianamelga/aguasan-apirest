package py.com.aguasan.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import py.com.aguasan.dto.UsuarioDTO;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "username")
	private String userName;

	@Column(name = "password")
	private String password;

	@Column(name = "nombre")
	private String nombre;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_tipo_documento")
	private TipoDocumento tipoDocument;

	@Column(name = "nro_documento")
	private String nroDocumento;

	@Column(name = "email")
	private String email;

	@Column(name = "es_admin")
	private String esAdmin;

	@Column(name = "activo")
	private String activo;

	@Column(name = "fec_alta")
	private Date fecAlta;

	@Column(name = "fec_ult_act")
	private Date fecUltAct;

	public Usuario() {
		super();
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoDocumento geTipoDocumento() {
		return tipoDocument;
	}

	public void setTipoDocumento(TipoDocumento tipoDocument) {
		this.tipoDocument = tipoDocument;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String isEsAdmin() {
		return esAdmin;
	}

	public void setEsAdmin(String esAdmin) {
		this.esAdmin = esAdmin;
	}

	public String isActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Date getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	public Date getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(Date fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	@Override
	public String toString() {
		return "Usuario [userName=" + userName + ", password=" + password + ", nombre=" + nombre + ", codTipoDocumento="
				+ tipoDocument + ", nroDocumento=" + nroDocumento + ", email=" + email + ", esAdmin=" + esAdmin
				+ ", activo=" + activo + ", fecAlta=" + fecAlta + ", fecUltAct=" + fecUltAct + "]";
	}

	public UsuarioDTO toDTO() {
		UsuarioDTO usuario = new UsuarioDTO();
		usuario.setUserName(this.userName);
		usuario.setPassword(this.password);
		usuario.setNombre(this.nombre);
		usuario.setTipoDocumento(this.tipoDocument.toDTO());
		usuario.setNroDocumento(this.nroDocumento);
		usuario.setEmail(this.email);
		usuario.setIsAdmin(this.esAdmin);
		usuario.setActivo(this.activo);
		usuario.setFecAlta((new FechaUtil()).dateToString(this.fecAlta));
		usuario.setFecUltAct((new FechaUtil()).dateToString(this.fecUltAct));
		return usuario;
	}

	public List<UsuarioDTO> toDTOList(List<Usuario> listaUsuarioEntity) {
		List<UsuarioDTO> listaUsuarioDTO = new ArrayList<UsuarioDTO>();
		for (Usuario usuario : listaUsuarioEntity) {
			listaUsuarioDTO.add(usuario.toDTO());
		}
		return listaUsuarioDTO;
	}

}