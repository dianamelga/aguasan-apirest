package py.com.aguasan.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import py.com.aguasan.dto.UbicacionClienteDTO;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Diana Raquel Melgarejo Velgara
 *
 */

@Entity
@Table(name = "ubicaciones_clientes")
public class UbicacionCliente {

	@Id
	@Column(name = "cod_ubicacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codUbicacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_tipo_ubicacion")
	private UbicacionTipo tipoUbicacion;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_cliente")
	@JsonIgnore
	private Cliente cliente;

	@Column(name = "latitud")
	private String latitud;

	@Column(name = "longitud")
	private String longitud;

	@Column(name = "fec_alta")
	private Date fecAlta;

	@Column(name = "usr_alta")
	private String usrAlta;

	@Column(name = "fec_ult_act")
	private Date fecUltAct;

	@Column(name = "usr_ult_act")
	private String usrUltAct;

	@Column(name = "referencia")
	private String referencia;

	public Long getCodUbicacion() {
		return codUbicacion;
	}

	public void setCodUbicacion(Long codUbicacion) {
		this.codUbicacion = codUbicacion;
	}

	public UbicacionTipo getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(UbicacionTipo tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public Date getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public Date getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(Date fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public String getReferencia() {
		return referencia.replace("'", " ");
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia.replace("'", " ");
	}

	public UbicacionClienteDTO toDTO() {
		UbicacionClienteDTO dto = new UbicacionClienteDTO();
		dto.setCodUbicacion(this.codUbicacion);
		dto.setCliente(this.cliente.toDTO());
		dto.setTipoUbicacion(this.tipoUbicacion);
		dto.setLatitud(this.latitud);
		dto.setLongitud(this.longitud);
		if (this.referencia != null) {
			dto.setReferencia(this.referencia.replace("'", " "));
		} else {
			dto.setReferencia(this.referencia);
		}
		dto.setFecAlta(new FechaUtil().dateToString(this.fecAlta));
		dto.setUsrAlta(this.usrAlta);
		dto.setFecUltAct(new FechaUtil().dateToString(this.fecUltAct));
		dto.setUsrUltAct(this.usrUltAct);

		return dto;
	}

}
