package py.com.aguasan.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.aguasan.dto.TipoDocumentoDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Entity
@Table(name = "tipo_documento")
public class TipoDocumento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cod_tipo_documento")
	private String codTipoDocumento;

	@Column(name = "tipo_documento")
	private String tipoDocumento;

	public TipoDocumento() {
		super();
	}

	public String getCodTipoDocumento() {
		return codTipoDocumento;
	}

	public void setCodTipoDocumento(String codTipoDocumento) {
		this.codTipoDocumento = codTipoDocumento;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public TipoDocumentoDTO toDTO() {
		TipoDocumentoDTO tipoDocumentoDTO = new TipoDocumentoDTO();
		tipoDocumentoDTO.setCodTipoDocumento(this.codTipoDocumento);
		tipoDocumentoDTO.setTipoDocumento(this.tipoDocumento);
		return tipoDocumentoDTO;
	}

	@Override
	public String toString() {
		return "TipoDocumento [codTipoDocumento=" + codTipoDocumento + ", tipoDocumento=" + tipoDocumento + "]";
	}

	public List<TipoDocumentoDTO> toDTOList(List<TipoDocumento> listaTipoDocumentosEntity) {
		List<TipoDocumentoDTO> listaTipoDocumentosDTO = new ArrayList<TipoDocumentoDTO>();
		for (TipoDocumento tipoDocumento : listaTipoDocumentosEntity) {
			listaTipoDocumentosDTO.add(tipoDocumento.toDTO());
		}
		return listaTipoDocumentosDTO;
	}

}
