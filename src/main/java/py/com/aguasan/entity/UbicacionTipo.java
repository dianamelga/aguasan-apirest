package py.com.aguasan.entity;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.aguasan.dto.UbicacionTipoDTO;

/**
 * @author Diana Raquel Melgarejo Velgara
 *
 */

@Entity
@Table(name = "ubicaciones_tipos")
public class UbicacionTipo {
	@Id
	@Column(name = "cod_tipo_ubicacion")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private BigInteger codTipoUbicacion;

	@Column(name = "tipo_ubicacion")
	private String tipoUbicacion;

	@Column(name = "fec_alta")
	private Date fecAlta;

	@Column(name = "usr_alta")
	private String usrAlta;

	@Column(name = "fec_ult_act")
	private Date fecUltAct;

	@Column(name = "usr_ult_act")
	private String usrUltAct;

	public BigInteger getCodTipoUbicacion() {
		return codTipoUbicacion;
	}

	public void setCodTipoUbicacion(BigInteger codTipoUbicacion) {
		this.codTipoUbicacion = codTipoUbicacion;
	}

	public String getTipoUbicacion() {
		return tipoUbicacion;
	}

	public void setTipoUbicacion(String tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public Date getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public Date getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(Date fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public UbicacionTipoDTO toDTO() {
		UbicacionTipoDTO dto = new UbicacionTipoDTO();
		dto.setCodTipoUbicacion(this.codTipoUbicacion);
		dto.setTipoUbicacion(this.tipoUbicacion);
		dto.setFecAlta(this.fecAlta.toString());
		dto.setUsrAlta(this.usrAlta);
		dto.setFecUltAct(this.fecUltAct.toString());
		dto.setUsrUltAct(this.usrUltAct);

		return dto;
	}

}
