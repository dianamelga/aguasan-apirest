package py.com.aguasan.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonInclude;

import py.com.aguasan.dto.BarrioDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Entity
@IdClass(BarrioPK.class)
@Table(name = "barrio")
public class Barrio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cod_barrio")
	private String codBarrio;

	@Column(name = "barrio")
	private String barrio;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_ciudad",  updatable= false)
	private Ciudad ciudad;

	public String getCodBarrio() {
		return codBarrio;
	}

	public void setCodBarrio(String codBarrio) {
		this.codBarrio = codBarrio;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public BarrioDTO toDTO() {
		BarrioDTO barrioDTO = new BarrioDTO();
		barrioDTO.setCodBarrio(this.codBarrio);
		barrioDTO.setBarrio(this.barrio);
		barrioDTO.setCiudad(this.ciudad.toDTO());
		return barrioDTO;
	}

	public List<BarrioDTO> toDTOList(List<Barrio> listaBarriosEntity) {
		List<BarrioDTO> listaBarriosDTO = new ArrayList<BarrioDTO>();
		for (Barrio barrio : listaBarriosEntity) {
			listaBarriosDTO.add(barrio.toDTO());
		}
		return listaBarriosDTO;
	}

}
