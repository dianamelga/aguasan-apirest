package py.com.aguasan.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.aguasan.dto.EventoDTO;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Entity
@Table(name = "evento")
public class Evento implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "nro_evento")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long nroEvento;

	@Column(name = "cod_cliente")
	private Long codCliente;

	@Column(name = "fec_evento")
	private Date fecEvento;

	@Column(name = "observacion")
	private String observacion;

	@Column(name = "estado")
	private String estado;

	@Column(name = "fec_alta")
	private Date fecAlta;

	@Column(name = "usr_alta")
	private String usrAlta;

	@Column(name = "fec_ult_act")
	private Date fecUltAct;

	@Column(name = "usr_ult_act")
	private String usrUltAct;

	public Long getNroEvento() {
		return nroEvento;
	}

	public void setNroEvento(Long nroEvento) {
		this.nroEvento = nroEvento;
	}

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public Date getFecEvento() {
		return fecEvento;
	}

	public void setFecEvento(Date fecEvento) {
		this.fecEvento = fecEvento;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Date getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public Date getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(Date fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public EventoDTO toDTO() {
		EventoDTO eventoDTO = new EventoDTO();
		eventoDTO.setNroEvento(this.nroEvento);
		eventoDTO.setCodCliente(this.codCliente);
		eventoDTO.setFecEvento(new FechaUtil().dateToString(this.fecEvento));
		eventoDTO.setEstado(this.estado);
		eventoDTO.setObservacion(this.observacion);
		eventoDTO.setFecAlta(new FechaUtil().dateToString(this.fecAlta));
		eventoDTO.setUsrAlta(this.usrAlta);
		eventoDTO.setFecUltAct(new FechaUtil().dateToString(this.fecUltAct));
		eventoDTO.setUsrUltAct(this.usrUltAct);
		return eventoDTO;
	}

	public List<EventoDTO> toDTOList(List<Evento> eventos) {
		List<EventoDTO> eventosDTO = new ArrayList<EventoDTO>();
		for (Evento evento : eventos) {
			eventosDTO.add(evento.toDTO());
		}
		return eventosDTO;
	}

}
