package py.com.aguasan.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import py.com.aguasan.dto.VisitaDTO;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Entity
@Table(name = "VISITA")
public class Visita implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "NRO_VISITA")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long nroVisita;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COD_CLIENTE")
	private Cliente cliente;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "COD_SITUACION")
	private SituacionVisita situacion;

	@Column(name = "BIDONES_CANJE")
	private BigInteger bidonesCanje;

	@Column(name = "BIDONES")
	private BigInteger bidones;

	@Column(name = "FEC_ALTA")
	private Date fecAlta;

	@Column(name = "USR_ALTA")
	private String usrAlta;

	@Column(name = "FEC_ULT_ACT")
	private Date fecUltAct;

	@Column(name = "usr_ULT_ACT")
	private String usrUltAct;

	public Long getNroVisita() {
		return nroVisita;
	}

	public void setNroVisita(Long nroVisita) {
		this.nroVisita = nroVisita;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public SituacionVisita getSituacion() {
		return situacion;
	}

	public void setSituacion(SituacionVisita situacion) {
		this.situacion = situacion;
	}

	public BigInteger getBidonesCanje() {
		return bidonesCanje;
	}

	public void setBidonesCanje(BigInteger bidonesCanje) {
		this.bidonesCanje = bidonesCanje;
	}

	public BigInteger getBidones() {
		return bidones;
	}

	public void setBidones(BigInteger bidones) {
		this.bidones = bidones;
	}

	public Date getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public Date getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(Date fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public VisitaDTO toDTO() {
		VisitaDTO visitaDTO = new VisitaDTO();
		visitaDTO.setNroVisita(this.nroVisita);
		visitaDTO.setCliente(this.cliente.toDTO());
		visitaDTO.setSituacion(this.situacion.toDTO());
		visitaDTO.setBidonesCanje(this.bidonesCanje.toString());
		visitaDTO.setBidones(this.bidones.toString());
		visitaDTO.setFecAlta(new FechaUtil().dateToString(this.fecAlta));
		visitaDTO.setUsrAlta(this.usrAlta);
		visitaDTO.setFecUltAct(new FechaUtil().dateToString(this.fecUltAct));
		visitaDTO.setUsrUltAct(this.usrUltAct);
		return visitaDTO;
	}

	public List<VisitaDTO> toDTOList(List<Visita> listaVisitaEntity) {
		List<VisitaDTO> listaVisitaDTO = new ArrayList<VisitaDTO>();
		for (Visita visitaEntity : listaVisitaEntity) {
			listaVisitaDTO.add(visitaEntity.toDTO());
		}
		return listaVisitaDTO;
	}

}
