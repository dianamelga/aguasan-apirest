package py.com.aguasan.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Entity
@Table(name = "usuario_acceso")
public class UsuarioAcceso implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "username")
	private String userName;

	@Column(name = "device_id")
	private String deviceId;

	@Column(name = "serial_instal")
	private BigInteger serialInstall;

	@Column(name = "fec_ult_act")
	private Date fechaUltimaAct;
	
	@Column(name = "token_registration_id")
	private String tokenRegistrationId;

	public String getUserName() {
		return userName;
	}

	public UsuarioAcceso() {
		super();
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public BigInteger getSerialInstall() {
		return serialInstall;
	}

	public void setSerialInstall(BigInteger serialInstall) {
		this.serialInstall = serialInstall;
	}

	public Date getFechaUltimaAct() {
		return fechaUltimaAct;
	}

	public void setFechaUltimaAct(Date fechaUltimaAct) {
		this.fechaUltimaAct = fechaUltimaAct;
	}
	
	

	public String getTokenRegistrationId() {
		return tokenRegistrationId;
	}

	public void setTokenRegistrationId(String tokenRegistrationId) {
		this.tokenRegistrationId = tokenRegistrationId;
	}

	@Override
	public String toString() {
		return "UsuarioAcceso [userName=" + userName + ", deviceId=" + deviceId + ", serialInstall=" + serialInstall
				+ ", fechaUltimaAct=" + fechaUltimaAct + "]";
	}

}
