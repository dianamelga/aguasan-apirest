package py.com.aguasan.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gson.Gson;

import py.com.aguasan.dto.ClienteDTO;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cod_cliente")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codCliente;

	@Column(name = "nombres")
	private String nombres;

	@Column(name = "apellidos")
	private String apellidos;

	@Column(name = "celular")
	private String celular;

	@Column(name = "telefono")
	private String telefono;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_tipo_documento")
	private TipoDocumento tipoDocumento;

	@Column(name = "nro_documento")
	private String nroDocumento;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({ @JoinColumn(name = "cod_ciudad"), @JoinColumn(name = "cod_barrio") })
	private Barrio barrio;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "observacion")
	private String observacion;

	@Column(name = "dias_visita")
	private String diasVisita;

	@Column(name = "activo")
	private String activo;

	@Column(name = "fec_alta")
	private Date fechaAlta;

	@Column(name = "usr_alta")
	private String userAlta;

	@Column(name = "fec_ult_act")
	private Date fechaUltimaAct;

	@Column(name = "usr_ult_act")
	private String userUltimaAct;

	public Cliente() {
		super();
	}

	public String getActivo() {
		return activo;
	}

	public void setActivo(String activo) {
		this.activo = activo;
	}

	public Long getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Long codCliente) {
		this.codCliente = codCliente;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public Barrio getBarrio() {
		return barrio;
	}

	public void setBarrio(Barrio barrio) {
		this.barrio = barrio;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getDiasVisita() {
		return diasVisita;
	}

	public void setDiasVisita(String diasVisita) {
		this.diasVisita = diasVisita;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getUserAlta() {
		return userAlta;
	}

	public void setUserAlta(String userAlta) {
		this.userAlta = userAlta;
	}

	public Date getFechaUltimaAct() {
		return fechaUltimaAct;
	}

	public void setFechaUltimaAct(Date fechaUltimaAct) {
		this.fechaUltimaAct = fechaUltimaAct;
	}

	public String getUserUltimaAct() {
		return userUltimaAct;
	}

	public void setUserUltimaAct(String userUltimaAct) {
		this.userUltimaAct = userUltimaAct;
	}

	@SuppressWarnings("unchecked")
	public ClienteDTO toDTO() {
		ClienteDTO clienteDTO = new ClienteDTO();
		clienteDTO.setCodCliente(this.codCliente);
		clienteDTO.setNombres(this.nombres);
		clienteDTO.setApellidos(this.apellidos);
		clienteDTO.setCelular(this.celular);
		clienteDTO.setTelefono(this.telefono);
		clienteDTO.setTipoDocumento(this.tipoDocumento.toDTO());
		clienteDTO.setNroDocumento(this.nroDocumento);
		clienteDTO.setCiudad(this.barrio.toDTO().getCiudad());
		clienteDTO.setBarrio(this.barrio.toDTO());
		clienteDTO.setDireccion(this.direccion);
		clienteDTO.setObservacion(this.observacion);
		clienteDTO.setDiasVisita((ArrayList<String>) (new Gson()).fromJson(this.diasVisita, ArrayList.class));
		clienteDTO.setActivo(this.activo);
		clienteDTO.setFechaAlta((new FechaUtil()).dateToString(this.fechaAlta));
		clienteDTO.setUserAlta(this.userAlta);
		clienteDTO.setFechaUltimaAct((new FechaUtil()).dateToString(this.fechaUltimaAct));
		clienteDTO.setUserUltimaAct(this.userUltimaAct);
		return clienteDTO;
	}

	@Override
	public String toString() {
		return "Cliente {codCliente:" + codCliente + ", nombres:" + nombres + ", apellidos:" + apellidos + ", celular:"
				+ celular + ", telefono:" + telefono + ", codTipoDocumento: {" + tipoDocumento + " }, nroDocumento:"
				+ nroDocumento + ", ciudad: { " + barrio.getCiudad() + " }, barrio: { " + barrio + " } , direccion:"
				+ direccion + ", observacion:" + observacion + ", diasVisita:" + diasVisita + ", activo:" + activo
				+ ", fechaAlta:" + fechaAlta + ", userAlta:" + userAlta + ", fechaUltimaAct:" + fechaUltimaAct
				+ ", userUltimaAct:" + userUltimaAct + "}";
	}

}
