package py.com.aguasan.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.aguasan.dto.CiudadDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Entity
@Table(name = "ciudad")
public class Ciudad implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "cod_ciudad")
	private String codCiudad;

	@Column(name = "ciudad")
	private String ciudad;

	public Ciudad() {
		super();
	}

	public String getCodCiudad() {
		return codCiudad;
	}

	public void setCodCiudad(String codCiudad) {
		this.codCiudad = codCiudad;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public CiudadDTO toDTO() {
		CiudadDTO ciudadDTO = new CiudadDTO();
		ciudadDTO.setCodCiudad(this.codCiudad);
		ciudadDTO.setCiudad(this.ciudad);
		return ciudadDTO;
	}

	@Override
	public String toString() {
		return "Ciudad [codCiudad=" + codCiudad + ", ciudad=" + ciudad + "]";
	}

	public List<CiudadDTO> toDTOList(List<Ciudad> listaCiudadesEntity) {
		List<CiudadDTO> listaCiudadesDTO = new ArrayList<CiudadDTO>();
		for (Ciudad ciudad : listaCiudadesEntity) {
			listaCiudadesDTO.add(ciudad.toDTO());
		}
		return listaCiudadesDTO;
	}

}
