package py.com.aguasan.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import py.com.aguasan.dto.SolicitudAccionDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Entity
@Table(name = "solicitud_accion")
public class SolicitudAccion implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id_accion")
	private BigInteger idAccion;

	@Column(name = "tipo")
	private String tipo;

	@Column(name = "objeto_json")
	private String objetoJson;

	@Column(name = "estado")
	private String estado;

	@Column(name = "id_remoto")
	private BigInteger idRemoto;

	@Column(name = "serial_instal")
	private BigInteger serialInstal;

	@Column(name = "fec_alta")
	private Date fechaAlta;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usr_alta", nullable = false)
	@NotNull
	private Usuario usuarioAlta;

	@Column(name = "fec_ult_act")
	private Date fechaUltAct;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "usr_ult_act", nullable = false)
	@NotNull
	private Usuario usuarioUltAct;

	public SolicitudAccion() {
		super();
	}

	public BigInteger getIdAccion() {
		return idAccion;
	}

	public void setIdAccion(BigInteger idAccion) {
		this.idAccion = idAccion;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getObjetoJson() {
		return objetoJson;
	}

	public void setObjetoJson(String objetoJson) {
		this.objetoJson = objetoJson;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public BigInteger getIdRemoto() {
		return idRemoto;
	}

	public void setIdRemoto(BigInteger idRemoto) {
		this.idRemoto = idRemoto;
	}

	public BigInteger getSerialInstal() {
		return serialInstal;
	}

	public void setSerialInstal(BigInteger serialInstal) {
		this.serialInstal = serialInstal;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Usuario getUsuarioAlta() {
		return usuarioAlta;
	}

	public void setUsuarioAlta(Usuario usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}

	public Date getFechaUltAct() {
		return fechaUltAct;
	}

	public void setFechaUltAct(Date fechaUltAct) {
		this.fechaUltAct = fechaUltAct;
	}

	public Usuario getUsuarioUltAct() {
		return usuarioUltAct;
	}

	public void setUsuarioUltAct(Usuario usuarioUltAct) {
		this.usuarioUltAct = usuarioUltAct;
	}

	public SolicitudAccionDTO toDTO() {
		SolicitudAccionDTO solicitudAccion = new SolicitudAccionDTO();
		solicitudAccion.setIdAccion(this.idAccion);
		solicitudAccion.setTipo(this.tipo);
		solicitudAccion.setObjetoJson(this.objetoJson);
		solicitudAccion.setEstado(this.estado);
		solicitudAccion.setIdRemoto(this.idRemoto.toString());
		solicitudAccion.setSerialInstal(this.serialInstal.toString());
		solicitudAccion.setFechaAlta(this.fechaAlta.toString());
		solicitudAccion.setUsuarioAlta(this.usuarioAlta.toDTO());
		solicitudAccion.setFechaUltAct(this.fechaUltAct.toString());
		solicitudAccion.setUsuarioUltAct(this.usuarioUltAct.toDTO());

		return solicitudAccion;
	}

}
