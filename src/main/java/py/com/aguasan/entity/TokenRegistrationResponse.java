package py.com.aguasan.entity;

import java.io.Serializable;

public class TokenRegistrationResponse implements Serializable {

	private String tokenRegistrationId;

	public String getTokenRegistrationId() {
		return tokenRegistrationId;
	}

	public void setTokenRegistrationId(String tokenRegistrationId) {
		this.tokenRegistrationId = tokenRegistrationId;
	}
	
	
}
