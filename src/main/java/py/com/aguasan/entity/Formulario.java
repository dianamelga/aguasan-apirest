package py.com.aguasan.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import py.com.aguasan.dto.FormularioDTO;
import py.com.aguasan.util.FechaUtil;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Entity
@Table(name = "formulario")
public class Formulario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "nro_formulario")
	private BigInteger nroFormulario;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "cod_cliente", nullable = false)
	@NotNull
	private Cliente codCliente;

	@Column(name = "bidones")
	private int bidones;

	@Column(name = "dispensers")
	private int dispensers;

	@Column(name = "bebederos")
	private int bebederos;

	@Column(name = "fec_alta")
	private Date fecAlta;

	@Column(name = "usr_alta")
	private String usrAlta;

	@Column(name = "fec_ult_act")
	private Date fecUltAct;

	@Column(name = "usr_ult_act")
	private String usrUltAct;

	public BigInteger getNroFormulario() {
		return nroFormulario;
	}

	public void setNroFormulario(BigInteger nroFormulario) {
		this.nroFormulario = nroFormulario;
	}

	public Cliente getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Cliente codCliente) {
		this.codCliente = codCliente;
	}

	public int getBidones() {
		return bidones;
	}

	public void setBidones(int bidones) {
		this.bidones = bidones;
	}

	public int getDispensers() {
		return dispensers;
	}

	public void setDispensers(int dispensers) {
		this.dispensers = dispensers;
	}

	public int getBebederos() {
		return bebederos;
	}

	public void setBebederos(int bebederos) {
		this.bebederos = bebederos;
	}

	public Date getFecAlta() {
		return fecAlta;
	}

	public void setFecAlta(Date fecAlta) {
		this.fecAlta = fecAlta;
	}

	public String getUsrAlta() {
		return usrAlta;
	}

	public void setUsrAlta(String usrAlta) {
		this.usrAlta = usrAlta;
	}

	public Date getFecUltAct() {
		return fecUltAct;
	}

	public void setFecUltAct(Date fecUltAct) {
		this.fecUltAct = fecUltAct;
	}

	public String getUsrUltAct() {
		return usrUltAct;
	}

	public void setUsrUltAct(String usrUltAct) {
		this.usrUltAct = usrUltAct;
	}

	public FormularioDTO toDTO() {
		FormularioDTO formularioDTO = new FormularioDTO();
		formularioDTO.setNroFormulario(this.nroFormulario);
		formularioDTO.setCodCliente(this.codCliente.toDTO());
		formularioDTO.setBidones(this.bidones);
		formularioDTO.setDispensers(this.dispensers);
		formularioDTO.setBebederos(this.bebederos);
		formularioDTO.setFecAlta(new FechaUtil().dateToString(this.fecAlta));
		formularioDTO.setUsrAlta(this.usrAlta);
		formularioDTO.setFecUltAct(new FechaUtil().dateToString(this.fecUltAct));
		formularioDTO.setUsrUltAct(this.usrUltAct);
		return formularioDTO;
	}

	public List<FormularioDTO> toDTOList(List<Formulario> formularios) {
		List<FormularioDTO> formulariosDTO = new ArrayList<FormularioDTO>();
		for (Formulario formulario : formularios) {
			formulariosDTO.add(formulario.toDTO());
		}
		return formulariosDTO;
	}
}
