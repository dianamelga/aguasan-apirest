package py.com.aguasan.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import py.com.aguasan.dto.SituacionVisitaDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Entity
@Table(name = "SITUACION_VISITA")
public class SituacionVisita implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "COD_SITUACION")
	public BigInteger codSituacion;

	@Column(name = "SITUACION")
	public String situacion;

	public BigInteger getCodSituacion() {
		return codSituacion;
	}

	public void setCodSituacion(BigInteger codSituacion) {
		this.codSituacion = codSituacion;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public SituacionVisitaDTO toDTO() {
		SituacionVisitaDTO situacionVisitaDTO = new SituacionVisitaDTO();
		situacionVisitaDTO.setCodSituacion(this.codSituacion.toString());
		situacionVisitaDTO.setSituacion(this.situacion);
		return situacionVisitaDTO;
	}

	public List<SituacionVisitaDTO> toDTOList(List<SituacionVisita> listaSituacionVisitaEntity) {
		List<SituacionVisitaDTO> listaSituacionVisitaDTO = new ArrayList<SituacionVisitaDTO>();
		for (SituacionVisita situacionVisita : listaSituacionVisitaEntity) {
			listaSituacionVisitaDTO.add(situacionVisita.toDTO());
		}
		return listaSituacionVisitaDTO;
	}

}
