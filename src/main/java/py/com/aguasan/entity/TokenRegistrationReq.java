package py.com.aguasan.entity;

import java.io.Serializable;

public class TokenRegistrationReq implements Serializable{

	private String tokenRegistrationId;
	private String userName;
	private String deviceId;
	
	
	public String getTokenRegistrationId() {
		return tokenRegistrationId;
	}
	public void setTokenRegistrationId(String tokenRegistrationId) {
		this.tokenRegistrationId = tokenRegistrationId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	
	
}
