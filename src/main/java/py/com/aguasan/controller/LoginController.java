package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.UserPassAccessDTO;

public interface LoginController {

	public String getJWTToken(String username);

	public ResponseEntity<Object> customerInformation(@RequestBody UserPassAccessDTO userPass);
}
