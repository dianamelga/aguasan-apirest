package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.entity.Mail;

public interface MailController {

	ResponseEntity<Object> sendMail(@RequestBody Mail mail);
	ResponseEntity<Object> sendDatabase(@RequestBody Mail mail);
}
