package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.VisitaDTO;

public interface VisitaController {

	public ResponseEntity<Object> createVisita(@RequestBody VisitaDTO visitaDTO);

	public ResponseEntity<Object> getListaVisita();

	public ResponseEntity<Object> getListaVisitaByCliente(@PathVariable("codCliente") Long codCliente);

	public ResponseEntity<Object> updateVisita(VisitaDTO visitaDTO);

}
