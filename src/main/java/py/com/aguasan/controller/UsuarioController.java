package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.UsuarioDTO;

public interface UsuarioController {

	public ResponseEntity<Object> createUsuario(@RequestBody UsuarioDTO usuarioDTO);

	public ResponseEntity<Object> updateUsuario(@RequestBody UsuarioDTO usuarioDTO);

	public ResponseEntity<Object> getUsuarios();

	public ResponseEntity<Object> getUsuario(@PathVariable("userName") String userName);

}
