package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.UserPassAccessDTO;

public interface AutenticacionController {

	public ResponseEntity<Object> getSerialInstall(@RequestBody UserPassAccessDTO userPass);

	public String getJWTToken(String username);

}
