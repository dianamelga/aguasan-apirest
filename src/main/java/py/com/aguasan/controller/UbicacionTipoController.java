package py.com.aguasan.controller;

import java.math.BigInteger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.UbicacionTipoDTO;

public interface UbicacionTipoController {

	public ResponseEntity<Object> createUbicacionTipo(@RequestBody UbicacionTipoDTO ubicacionTipoDTO);

	public ResponseEntity<Object> updateUbicacionTipo(@RequestBody UbicacionTipoDTO ubicacionTipoDTO);

	public ResponseEntity<Object> getListaUbicacionTipo();

	public ResponseEntity<Object> getUbicacionTipo(@PathVariable("codTipoUbicacion") BigInteger codTipoUbicacion);

	public ResponseEntity<Object> downUbicacionTipo(@RequestBody BigInteger codTipoUbicacion);

	public ResponseEntity<Object> upUbicacionTipo(@RequestBody BigInteger codTipoUbicacion);
}
