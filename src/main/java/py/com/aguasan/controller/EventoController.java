package py.com.aguasan.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.EventoDTO;
import py.com.aguasan.entity.Evento;

/**
 * 
 * @author Luis Fernando Capdevila Avalos
 *
 */
public interface EventoController {

	public ResponseEntity<Object> getEventos();

	public ResponseEntity<Object> getEventos(String estado);
	
	public ResponseEntity<Object> getEventos(String estado, String fecEvento);

	public ResponseEntity<Object> createEvento(@RequestBody EventoDTO eventoDTO);

	public ResponseEntity<Object> updateEvento(@RequestBody EventoDTO eventoDTO);

	public ResponseEntity<Object> getEvento(@PathVariable("nroEvento") Long nroEvento);

	public ResponseEntity<Object> deleteEvento(@PathVariable("nroEvento") Long nroEvento);

}
