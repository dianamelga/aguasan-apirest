package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.BarrioDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface BarrioController {

	public ResponseEntity<Object> getBarrios();

	public ResponseEntity<Object> createBarrio(@RequestBody BarrioDTO barrioDTO);

	public ResponseEntity<Object> updateBarrio(@RequestBody BarrioDTO barrioDTO);

	public ResponseEntity<Object> getBarrio(@PathVariable("codBarrio") String codBarrio,
			@PathVariable("codCiudad") String codCiudad);

	public ResponseEntity<Object> deleteBarrio(@PathVariable("codBarrio") String codBarrio,
			@PathVariable("codCiudad") String codCiudad);

}
