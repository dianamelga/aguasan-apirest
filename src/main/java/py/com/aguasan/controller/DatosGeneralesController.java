package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;

public interface DatosGeneralesController {

	public ResponseEntity<Object> getBarrios();

	public ResponseEntity<Object> getCiudades();

	public ResponseEntity<Object> getTiposDocumentos();

	public ResponseEntity<Object> getListaUbicacionTipo();

	public ResponseEntity<Object> getSituacionVisita();
}
