package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.CiudadDTO;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

public interface CiudadController {

	public ResponseEntity<Object> getCiudads();

	public ResponseEntity<Object> createCiudad(@RequestBody CiudadDTO ciudadDTO);

	public ResponseEntity<Object> updateCiudad(@RequestBody CiudadDTO ciudadDTO);

	public ResponseEntity<Object> getCiudad(@PathVariable("codCiudad") String codCiudad);
	
	public ResponseEntity<Object> deleteCiudad(@PathVariable("codCiudad") String codCiudad);

}
