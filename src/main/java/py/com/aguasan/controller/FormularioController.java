package py.com.aguasan.controller;

import java.math.BigInteger;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.FormularioDTO;

public interface FormularioController {

	public ResponseEntity<Object> createFormulario(@RequestBody FormularioDTO formularioDTO);

	public ResponseEntity<Object> getListaFormulario();

	public ResponseEntity<Object> getFormularioByNroDocumento(@PathVariable("nroDocumento") String nroDocumento);

	public ResponseEntity<Object> getFormularioByCodCliente(@PathVariable("codCliente") Long codCliente);

	public ResponseEntity<Object> getFormulario(@PathVariable("nroFormulario") BigInteger nroFormulario);

}
