package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.ClienteDTO;

public interface ClienteController {

	public ResponseEntity<Object> createCliente(@RequestBody ClienteDTO clienteDTO);

	public ResponseEntity<Object> updateCliente(@RequestBody ClienteDTO clienteDTO);

	public ResponseEntity<Object> getClientes();

	public ResponseEntity<Object> getCliente(@PathVariable("codCliente") Long codCliente);

	public ResponseEntity<Object> downCliente(@RequestBody Long codCliente);

	public ResponseEntity<Object> upCliente(@RequestBody Long codCliente);

	public ResponseEntity<Object> deleteUbicacionCliente(@PathVariable("codUbicacion") Long codUbicacion);

	public ResponseEntity<Object> deleteCliente(Long codCliente);

}
