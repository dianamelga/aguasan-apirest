package py.com.aguasan.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import py.com.aguasan.entity.Mail;
import py.com.aguasan.entity.ReportesReq;
import py.com.aguasan.report.Reporte;
import py.com.aguasan.entity.GenericApiResponse;
import py.com.aguasan.service.IClienteService;
import py.com.aguasan.service.IFormularioService;
import py.com.aguasan.service.IMailService;
import py.com.aguasan.service.IVisitaService;
import py.com.aguasan.util.ManejoDeArchivos;

@Controller
@RestController
@RequestMapping(value = "aguasan-reportes")
public class ReportesController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ReportesController.class);
	@Autowired
	private IClienteService iClienteService;

	@Autowired
	private IFormularioService iformularioService;

	@Autowired
	private IVisitaService iVisitaService;

	@Autowired
	private IMailService mailService;

	private ManejoDeArchivos manejoDeArchivos = new ManejoDeArchivos();
	
	
	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> sendReports(@RequestBody ReportesReq reportesReq) {
		try {
		
			List<Reporte> reports = new ArrayList<Reporte>();

			Reporte clientesReport = iClienteService.getClientesToExport();
			Reporte formulariosReport = iformularioService.getFormularioToExport();
			Reporte visitasReport = iVisitaService.getVisitasReport();
			Reporte ubicacionesClientesReport = iClienteService.getUbicacionesClientesToExport();
			
			reports.add(clientesReport);
			reports.add(formulariosReport);
			reports.add(visitasReport);
			reports.add(ubicacionesClientesReport);
			
			//guardamos en una lista los paths de los reportes
			List<String> reportsPathFile = new ArrayList<String>();

			
			for (Reporte report : reports) {
				report.toCSV();
				reportsPathFile.add(report.getFilePath());
			}

			String zipFilePath = manejoDeArchivos.toZip(reportsPathFile);

			Mail mail = new Mail();
			mail.setDestination(reportesReq.getMailDestino());
			mail.setSubject("Reportes - AguaSan");
			mail.setAttachmentName("Reportes_AguaSan.zip");
			mail.setAttachment(zipFilePath);
			mailService.sendMail(mail);

		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());

		}
		GenericApiResponse response = new GenericApiResponse();
		response.setMessage("Los reportes fueron enviados a: "+reportesReq.getMailDestino());
		
		return ResponseEntity.status(HttpStatus.OK).body(response);

	}

}
