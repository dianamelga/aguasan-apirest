package py.com.aguasan.controller;



import org.springframework.http.ResponseEntity;

import py.com.aguasan.entity.AguaSanMessage;
import py.com.aguasan.entity.TokenRegistrationReq;

public interface FirebaseAdminController {
	ResponseEntity<Object> sendPushNotification(AguaSanMessage message);
	ResponseEntity<Object>  registerToken(TokenRegistrationReq registrationTokenReq);
	ResponseEntity<Object> getTokenRegistrationId(String userName);
	ResponseEntity<Object> getAllTokensRegistrationId();

}
