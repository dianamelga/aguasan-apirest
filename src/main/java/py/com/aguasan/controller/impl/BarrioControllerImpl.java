package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.BarrioController;
import py.com.aguasan.dto.BarrioDTO;
import py.com.aguasan.entity.GenericApiResponse;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IBarrioService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Controller
@RestController
@RequestMapping(value = "barrios")
public class BarrioControllerImpl implements BarrioController {

	private static final Logger LOGGER = LoggerFactory.getLogger(BarrioControllerImpl.class);

	@Autowired
	private IBarrioService iBarrioService;

	private AguaSanError error = new AguaSanError();

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getBarrios() {
		try {
			String resultado = iBarrioService.getBarrios();
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createBarrio(@RequestBody BarrioDTO barrioDTO) {
		try {
			String resultado = iBarrioService.createBarrio(barrioDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch(DataIntegrityViolationException ex) {
			LOGGER.error(ex.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage("Ya existe el Barrio con código: "+barrioDTO.getCodBarrio()+
					" perteneciente a Ciudad con código: "+barrioDTO.getCiudad().getCodCiudad());
			error.setUseApiMessage(true);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
		}catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateBarrio(@RequestBody BarrioDTO barrioDTO) {
		try {
			String resultado = iBarrioService.updateBarrio(barrioDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}


	@JsonFormat
	@RequestMapping(value = "/{codBarrio}/{codCiudad}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getBarrio(@PathVariable("codBarrio") String codBarrio, @PathVariable("codCiudad") String codCiudad ) {
		try {
			String resultado = iBarrioService.getBarrio(codBarrio, codCiudad);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{codBarrio}/{codCiudad}", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> deleteBarrio(@PathVariable("codBarrio") String codBarrio, @PathVariable("codCiudad") String codCiudad) {
		try {
			iBarrioService.deleteBarrio(codBarrio, codCiudad);
			GenericApiResponse response = new GenericApiResponse();
			response.setMessage("La ciudad con código "+codCiudad+ " fué eliminada correctamente.");
			LOGGER.info("\n\n" + response);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		} catch(DataIntegrityViolationException ex) {
			LOGGER.error(ex.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage("No puede eliminar el Barrio con código: "+codBarrio+
					" perteneciente a Ciudad con código: "+codCiudad+ " porque esta ya se encuentra en uso.");
			error.setUseApiMessage(true);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
		}catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

}
