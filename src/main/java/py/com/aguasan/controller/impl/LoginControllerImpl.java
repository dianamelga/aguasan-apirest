package py.com.aguasan.controller.impl;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import py.com.aguasan.controller.LoginController;
import py.com.aguasan.dto.SerialTokenDTO;
import py.com.aguasan.dto.UserPassAccessDTO;
import py.com.aguasan.entity.Usuario;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IUsuarioService;

@Controller
@RestController
@RequestMapping(value = "/aguasan-login")
public class LoginControllerImpl implements LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginControllerImpl.class);
	@Autowired
	private IUsuarioService iUsuarioService;

	@Value("${jwt.key.private}")
	private String secretKey;

	@Value("${jwt.key.time.millis}")
	private int tiempoDeVidaDelToken;

	@JsonFormat
	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> customerInformation(@RequestBody UserPassAccessDTO userPass) {
		AguaSanError error = new AguaSanError();
		try {
			// validamos user y password
			String usuarioGson = iUsuarioService.getUsuario(userPass.getUserName(), userPass.getPassword());
			Usuario usuario = new Gson().fromJson(usuarioGson, Usuario.class);
			// generamos token para el usuario
			String token = getJWTToken(userPass.getUserName());

			SerialTokenDTO serialToken = new SerialTokenDTO(null, token, usuario.isEsAdmin().equals("S"), usuario.getEmail());
			return ResponseEntity.status(HttpStatus.OK).body(serialToken);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-0001");
			error.setMessage("Usuario o contraseña incorrectos.");
			error.setUseApiMessage(true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}

	}

	public String getJWTToken(String username) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId("softtekJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + tiempoDeVidaDelToken))
				.signWith(SignatureAlgorithm.HS512, this.secretKey.getBytes()).compact();

		return token;
	}
}
