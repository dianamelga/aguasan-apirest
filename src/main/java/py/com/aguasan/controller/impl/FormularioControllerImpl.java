package py.com.aguasan.controller.impl;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.FormularioController;
import py.com.aguasan.dto.FormularioDTO;
import py.com.aguasan.service.IFormularioService;

@Controller
@RestController
@RequestMapping(value = "/formulario")
public class FormularioControllerImpl implements FormularioController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FormularioControllerImpl.class);

	@Autowired
	private IFormularioService iFormularioService;

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createFormulario(@RequestBody FormularioDTO formularioDTO) {
		try {
			String resultSet = iFormularioService.createFormulario(formularioDTO);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (DataIntegrityViolationException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getListaFormulario() {
		try {
			String resultSet = iFormularioService.getFormularios();
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{nroDocumento}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getFormularioByNroDocumento(@PathVariable("nroDocumento") String nroDocumento) {
		try {
			String resultado = iFormularioService.getFormularioByNroDocumento(nroDocumento);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());

		}
	}

	@JsonFormat
	@RequestMapping(value = "/{codCliente}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getFormularioByCodCliente(@PathVariable("codCliente") Long codCliente) {
		try {
			String resultado = iFormularioService.getFormularioByCodCliente(codCliente);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());

		}
	}

	@JsonFormat
	@RequestMapping(value = "/{nroFormulario}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getFormulario(@PathVariable("nroFormulario") BigInteger nroFormulario) {
		try {
			String resultado = iFormularioService.getFormulario(nroFormulario);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());

		}
	}

}
