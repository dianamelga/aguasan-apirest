package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.ClienteController;
import py.com.aguasan.dto.ClienteDTO;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.error.AguasanException;
import py.com.aguasan.service.IClienteService;

@Controller
@RestController
@RequestMapping(value = "aguasan-cliente")
public class ClienteControllerImpl implements ClienteController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClienteControllerImpl.class);

	@Autowired
	private IClienteService iClienteService;

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<Object> createCliente(@RequestBody ClienteDTO clienteDTO) {
		try {
			String resultado = iClienteService.createCliente(clienteDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (DataIntegrityViolationException e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0300", "El Cliente ya existe, verifique los datos", true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);

		} catch (AguasanException e) {
			e.printStackTrace();
			LOGGER.error(e.getMensaje());
			AguaSanError error = new AguaSanError("ag-0305", e.getMensaje(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}

	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateCliente(@RequestBody ClienteDTO clienteDTO) {
		try {
			String resultado = iClienteService.updateCliente(clienteDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0301", "Codigo de cliente inexistente, verifique los datos",
					true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (AguasanException e) {
			e.printStackTrace();
			LOGGER.error(e.getMensaje());
			AguaSanError error = new AguaSanError("ag-0305", e.getMensaje(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getClientes() {
		try {
			String resultado = iClienteService.getClientes(false);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0302", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}

	}

	@JsonFormat
	@RequestMapping(value = "/{codCliente}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getCliente(@PathVariable("codCliente") Long codCliente) {
		try {
			String resultado = iClienteService.getCliente(codCliente);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0303", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/deactivate/{codCliente}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> downCliente(@PathVariable("codCliente") Long codCliente) {
		try {
			String resultado = iClienteService.downCliente(codCliente);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0304", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/activate/{codCliente}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> upCliente(@PathVariable("codCliente") Long codCliente) {
		try {
			String resultado = iClienteService.upCliente(codCliente);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0305", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "ubicaciones/{codUbicacion}", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> deleteUbicacionCliente(@PathVariable("codUbicacion") Long codUbicacion) {
		try {
			int resultado = iClienteService.deleteUbicacionCliebnte(codUbicacion);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0303", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{codCliente}", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> deleteCliente(@PathVariable("codCliente") Long codCliente) {
		try {
			iClienteService.deleteCliente(codCliente);
			String resultado = "\n\nCliente "+codCliente+ " eliminado";
			AguaSanError message = new AguaSanError("200", resultado, true);
			LOGGER.info(resultado);
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0303", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

}
