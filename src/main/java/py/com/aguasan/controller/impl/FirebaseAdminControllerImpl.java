package py.com.aguasan.controller.impl;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.FirebaseAdminController;
import py.com.aguasan.entity.AguaSanMessage;
import py.com.aguasan.entity.TokenRegistrationReq;
import py.com.aguasan.entity.TokenRegistrationResponse;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IFirebaseAdminService;
import py.com.aguasan.service.IUsuarioAccesoService;

@Controller
@RestController
@RequestMapping(value = "firebase")
public class FirebaseAdminControllerImpl implements FirebaseAdminController{

	private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudAccionControllerImpl.class);

	@Autowired
	private IFirebaseAdminService iFirebaseAdminService;

	private AguaSanError error = new AguaSanError();

	
	@JsonFormat
	@RequestMapping(value = "/notifications", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> sendPushNotification(@RequestBody AguaSanMessage message) {
		try {
			iFirebaseAdminService.sendPushNotification(message);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
		
		error.setCode("200");
		error.setMessage("Notification sent");
		error.setUseApiMessage(false);
		return ResponseEntity.status(HttpStatus.OK).body(error);
	}
	
	@JsonFormat
	@RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> registerToken(@RequestBody TokenRegistrationReq request) {
		try {
			iFirebaseAdminService.registerToken(request);
			TokenRegistrationResponse response = new TokenRegistrationResponse();
			response.setTokenRegistrationId(iFirebaseAdminService.getTokenRegistrationId(request.getUserName()));
			return ResponseEntity.status(HttpStatus.OK).body(response);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
		
	
	}

	@JsonFormat
	@RequestMapping(value = "/registration/{userName}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getTokenRegistrationId(@PathVariable("userName") String userName) {
		try {
			
			TokenRegistrationResponse response = new TokenRegistrationResponse();
			response.setTokenRegistrationId(iFirebaseAdminService.getTokenRegistrationId(userName));
			
			return ResponseEntity.status(HttpStatus.OK).body(response);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
		
		
	}

	@JsonFormat
	@RequestMapping(value = "/registration", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getAllTokensRegistrationId() {
		try {
			ArrayList<TokenRegistrationResponse> response = new ArrayList<TokenRegistrationResponse>();
			ArrayList<String> tokens = iFirebaseAdminService.getAllTokensRegistrationId();
			
			for(String token : tokens) {
				TokenRegistrationResponse tokenReg = new TokenRegistrationResponse();
				tokenReg.setTokenRegistrationId(token);
				response.add(tokenReg);
			}
			
			return ResponseEntity.status(HttpStatus.OK).body(response);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}
	
	
	
	
	
	

	

}
