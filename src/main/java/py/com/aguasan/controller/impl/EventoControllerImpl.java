package py.com.aguasan.controller.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.EventoController;
import py.com.aguasan.dto.EventoDTO;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IEventoService;
import py.com.aguasan.util.FechaUtil;

/**
 * 
 * @author Luis Fernando Capdevila Avalos
 *
 */
@Controller
@RestController
@RequestMapping(value = "evento")
public class EventoControllerImpl implements EventoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudAccionControllerImpl.class);

	@Autowired
	private IEventoService iEventoService;

	private AguaSanError error = new AguaSanError();

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getEventos() {
		try {
			String resultSet = iEventoService.getEventos();
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createEvento(@RequestBody EventoDTO eventoDTO) {
		try {
			String resultSet = iEventoService.createEvento(eventoDTO);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateEvento(@RequestBody EventoDTO eventoDTO) {
		try {
			String resultSet = iEventoService.updateEvento(eventoDTO);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{nroEvento}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getEvento(@PathVariable("nroEvento") Long nroEvento) {
		try {
			String resultado = iEventoService.getEvento(nroEvento);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{nroEvento}", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> deleteEvento(@PathVariable("nroEvento") Long nroEvento) {
		try {
			iEventoService.deleteEvento(nroEvento);
			String resultado = ":D";
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{estado}", method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getEventos(String estado) {
		try {
			String resultSet = iEventoService.getEventos(estado);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{estado}/{fecEvento}",method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getEventos(@PathVariable("estado") String estado, @PathVariable("fecEvento") String fecEvento) {
		try {
			Date fecEventoDate = new FechaUtil().stringToDateParser(fecEvento);
			String resultSet = iEventoService.getEventos(estado, fecEventoDate);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

}
