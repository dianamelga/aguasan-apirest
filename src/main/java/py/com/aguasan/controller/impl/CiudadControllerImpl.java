package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.CiudadController;
import py.com.aguasan.dto.CiudadDTO;
import py.com.aguasan.entity.GenericApiResponse;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.ICiudadService;

/**
 * @author Luis Fernando Capdevila Avalos
 *
 */

@Controller
@RestController
@RequestMapping(value = "ciudades")
public class CiudadControllerImpl implements CiudadController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CiudadControllerImpl.class);

	@Autowired
	private ICiudadService iCiudadService;

	private AguaSanError error = new AguaSanError();

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getCiudads() {
		try {
			String resultado = iCiudadService.getCiudades();
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createCiudad(@RequestBody CiudadDTO ciudadDTO) {
		try {
			String resultado = iCiudadService.createCiudad(ciudadDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch(DataIntegrityViolationException ex) {
			LOGGER.error(ex.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage("Ya existe una Ciudad con código: "+ciudadDTO.getCodCiudad());
			error.setUseApiMessage(true);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateCiudad(@RequestBody CiudadDTO ciudadDTO) {
		try {
			String resultado = iCiudadService.updateCiudad(ciudadDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{codCiudad}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getCiudad(@PathVariable("codCiudad") String codCiudad) {
		try {
			String resultado = iCiudadService.getCiudad(codCiudad);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	
	@JsonFormat
	@RequestMapping(value = "/{codCiudad}", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> deleteCiudad(@PathVariable("codCiudad") String codCiudad) {
		try {
			iCiudadService.deleteCiudad(codCiudad);
			GenericApiResponse response = new GenericApiResponse();
			response.setMessage("La ciudad con código "+codCiudad+ " fué eliminada correctamente.");
			LOGGER.info("\n\n" + response);
			return ResponseEntity.status(HttpStatus.OK).body(response);
		} catch(DataIntegrityViolationException ex) {
			LOGGER.error(ex.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage("No puede eliminar la Ciudad con código: "+codCiudad+ " porque esta ya se encuentra en uso.");
			error.setUseApiMessage(true);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
		}catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

}
