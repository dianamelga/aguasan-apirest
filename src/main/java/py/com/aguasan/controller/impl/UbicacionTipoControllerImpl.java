package py.com.aguasan.controller.impl;

import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.UbicacionTipoController;
import py.com.aguasan.dto.UbicacionTipoDTO;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IUbicacionTipoService;

@Controller
@RestController
@RequestMapping(value = "aguasan-ubicaciones-tipos")
public class UbicacionTipoControllerImpl implements UbicacionTipoController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UbicacionTipoControllerImpl.class);

	@Autowired
	private IUbicacionTipoService iUbicacionTipoService;

	private AguaSanError error = new AguaSanError();

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createUbicacionTipo(@RequestBody UbicacionTipoDTO ubicacionesTiposDTO) {
		try {
			String resultado = iUbicacionTipoService.createUbicacionTipo(ubicacionesTiposDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}

	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateUbicacionTipo(UbicacionTipoDTO ubicacionesTiposDTO) {
		try {
			String resultado = iUbicacionTipoService.updateUbicacionTipo(ubicacionesTiposDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{codTipoUbicacion}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getUbicacionTipo(@PathVariable("codTipoUbicacion") BigInteger codTipoUbicacion) {
		try {
			String resultado = iUbicacionTipoService.getUbicacionTipo(codTipoUbicacion);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> downUbicacionTipo(BigInteger codTipoUbicacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@JsonFormat
	@RequestMapping(value = "/up-ubicaciones-tipos", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> upUbicacionTipo(BigInteger codTipoUbicacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<Object> getListaUbicacionTipo() {
		// TODO Auto-generated method stub
		return null;
	}

}
