package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.SolicitudAccionController;
import py.com.aguasan.dto.SolicitudAccionDTO;
import py.com.aguasan.service.ISolicitudAccionService;

@Controller
@RestController
@RequestMapping(value = "/solicitud-accion")
public class SolicitudAccionControllerImpl implements SolicitudAccionController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SolicitudAccionControllerImpl.class);

	@Autowired
	private ISolicitudAccionService iSolicitudAccionService;

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createSolicitudAccion(@RequestBody SolicitudAccionDTO solicitudAccionDTO) {
		try {
			String resultSet = iSolicitudAccionService.createSolicidutAccion(solicitudAccionDTO);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}

	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getListaSolicitudAccion() {
		try {
			String resultSet = iSolicitudAccionService.getListaSolicitudAccion();
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (EmptyResultDataAccessException e) {
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateSolicitudAccion(@RequestBody SolicitudAccionDTO solicitudAccionDTO) {
		if (solicitudAccionDTO != null) {
			try {
				String resultSet = iSolicitudAccionService.updateSolicidutesAcciones(solicitudAccionDTO);
				return ResponseEntity.status(HttpStatus.OK).body(resultSet);
			} catch (EmptyResultDataAccessException e) {
				LOGGER.error(e.getLocalizedMessage());
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
			}
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

		}

	}

}
