package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.UsuarioController;
import py.com.aguasan.dto.UsuarioDTO;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IUsuarioService;

@Controller
@RestController
@RequestMapping(value = "aguasan-usuario")
public class UsuarioControllerImpl implements UsuarioController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioControllerImpl.class);

	@Autowired
	private IUsuarioService iUsuarioService;

	private AguaSanError error = new AguaSanError();

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createUsuario(@RequestBody UsuarioDTO usuarioDTO) {
		try {
			String resultado = iUsuarioService.createUsuario(usuarioDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateUsuario(@RequestBody UsuarioDTO usuarioDTO) {
		try {
			String resultado = iUsuarioService.updateUsuario(usuarioDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getUsuarios() {
		try {
			String resultado = iUsuarioService.getAllUsers();
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@Override
	public ResponseEntity<Object> getUsuario(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

}
