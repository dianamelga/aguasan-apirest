package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.MailController;
import py.com.aguasan.entity.Mail;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IMailService;

@Controller
@RestController
@RequestMapping(value = "aguasan-mail")
public class MailControllerImpl implements MailController {
	private static final Logger LOGGER = LoggerFactory.getLogger(MailControllerImpl.class);
	private AguaSanError error = new AguaSanError();
	@Autowired
	private IMailService mailService;

	@JsonFormat
	@RequestMapping(value = "/mail", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> sendMail(@RequestBody Mail mail) {
		try {
			String resultado = mailService.sendMail(mail);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}

	}
	
	@JsonFormat
	@RequestMapping(value = "/database", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> sendDatabase(@RequestBody Mail mail) {
		try {
			String resultado = mailService.sendDatabase(mail);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			error.setCode("ag-xxxx");
			error.setMessage(e.getLocalizedMessage());
			error.setUseApiMessage(false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}

	}

}
