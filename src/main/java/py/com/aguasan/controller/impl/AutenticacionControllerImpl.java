package py.com.aguasan.controller.impl;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.Gson;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import py.com.aguasan.controller.AutenticacionController;
import py.com.aguasan.dto.SerialTokenDTO;
import py.com.aguasan.dto.UserPassAccessDTO;
import py.com.aguasan.entity.Usuario;
import py.com.aguasan.entity.UsuarioAcceso;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IUsuarioAccesoService;
import py.com.aguasan.service.IUsuarioService;

@Controller
@RestController
@RequestMapping(value = "aguasan-auth")
public class AutenticacionControllerImpl implements AutenticacionController {

	@Autowired
	private IUsuarioService iUsuarioService;

	@Value("${jwt.key.private}")
	private String secretKey;

	@Value("${jwt.key.time.millis}")
	private int tiempoDeVidaDelToken;

	@Autowired
	private IUsuarioAccesoService iUsuarioAccesoService;

	public AguaSanError error = null;

	private static final Logger LOGGER = LoggerFactory.getLogger(AutenticacionControllerImpl.class);

	@JsonFormat
	@RequestMapping(value = "/serial", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public ResponseEntity<Object> getSerialInstall(@RequestBody UserPassAccessDTO userPass) {
		try {

			// validamos user y password

			String usuarioGson = iUsuarioService.getUsuario(userPass.getUserName(), userPass.getPassword());
			Usuario usuario = new Gson().fromJson(usuarioGson, Usuario.class);
			// generamos token para el usuario
			String token = getJWTToken(userPass.getUserName());
			SerialTokenDTO serialToken = new SerialTokenDTO(
					new BigInteger(iUsuarioAccesoService.generateSerialInstall()), token,
					usuario.isEsAdmin().equals("S"), usuario.getEmail());
			UsuarioAcceso usuarioAcceso = new UsuarioAcceso();
			usuarioAcceso.setUserName(userPass.getUserName());
			usuarioAcceso.setSerialInstall(serialToken.getSerial());
			usuarioAcceso.setDeviceId(userPass.getDeviceId());
			usuarioAcceso.setTokenRegistrationId(userPass.getTokenRegistrationId());
			usuarioAcceso.setFechaUltimaAct(new Date());
			if (iUsuarioAccesoService.getUsuarioAcceso(userPass.getUserName()) != null) {
				iUsuarioAccesoService.updateUsuarioAcceso(usuarioAcceso);
			} else {
				iUsuarioAccesoService.createUsuarioAcceso(usuarioAcceso);

			}
			return ResponseEntity.status(HttpStatus.OK).body(serialToken);

		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getClass().getCanonicalName() + "; " + e.getLocalizedMessage());
			error = new AguaSanError("ag-0001", "Usuario o contraseña incorrectos.", true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getClass().getCanonicalName() + "; " + e.getLocalizedMessage());
			error = new AguaSanError("ag-000E", e.getMessage(), false);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	public String getJWTToken(String username) {
		List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

		String token = Jwts.builder().setId("softtekJWT").setSubject(username)
				.claim("authorities",
						grantedAuthorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + tiempoDeVidaDelToken))
				.signWith(SignatureAlgorithm.HS512, this.secretKey.getBytes()).compact();

		return token;
	}

}