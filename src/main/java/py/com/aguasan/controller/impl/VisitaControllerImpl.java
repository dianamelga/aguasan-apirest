package py.com.aguasan.controller.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;

import py.com.aguasan.controller.VisitaController;
import py.com.aguasan.dto.VisitaDTO;
import py.com.aguasan.error.AguaSanError;
import py.com.aguasan.service.IVisitaService;

@Controller
@RestController
@RequestMapping(value = "visita")
public class VisitaControllerImpl implements VisitaController {

	private static final Logger LOGGER = LoggerFactory.getLogger(VisitaControllerImpl.class);

	@Autowired
	private IVisitaService iVisitaService;

	@JsonFormat
	@RequestMapping(method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> createVisita(@RequestBody VisitaDTO visitaDTO) {
		try {
			String resultSet = iVisitaService.createVisita(visitaDTO);
			LOGGER.info(resultSet);
			return ResponseEntity.status(HttpStatus.OK).body(resultSet);
		} catch (DataIntegrityViolationException e) {
			AguaSanError error = new AguaSanError("ag-0700", "El registro ya existe, verifique los datos", true);
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (Exception e) {
			LOGGER.error(e.getLocalizedMessage());
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getListaVisita() {
		try {
			String resultado = iVisitaService.getListaVisita();
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0702", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

	@JsonFormat
	@RequestMapping(method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> updateVisita(VisitaDTO visitaDTO) {
		try {
			String resultado = iVisitaService.updateVisita(visitaDTO);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0701", "Inexistente, verifique los datos", true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

	@JsonFormat
	@RequestMapping(value = "/{codCliente}", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	@Override
	public ResponseEntity<Object> getListaVisitaByCliente(@PathVariable("codCliente") Long codCliente) {
		try {
			String resultado = iVisitaService.getListaVisitaByCliente(codCliente);
			LOGGER.info("\n\n" + resultado);
			return ResponseEntity.status(HttpStatus.OK).body(resultado);
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error(e.getLocalizedMessage());
			AguaSanError error = new AguaSanError("ag-0702", e.getMessage(), true);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
		}
	}

}
