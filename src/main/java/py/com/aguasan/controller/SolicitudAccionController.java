package py.com.aguasan.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import py.com.aguasan.dto.SolicitudAccionDTO;

public interface SolicitudAccionController {

	public ResponseEntity<Object> createSolicitudAccion(@RequestBody SolicitudAccionDTO solicitudAccionDTO);

	public ResponseEntity<Object> getListaSolicitudAccion();

	public ResponseEntity<Object> updateSolicitudAccion(SolicitudAccionDTO solicitudAccionDTO);

}
