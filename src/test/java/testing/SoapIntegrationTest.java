//package testing;
//
//import static org.assertj.core.api.Assertions.assertThat;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.context.embedded.LocalServerPort;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
//import org.springframework.oxm.jaxb.Jaxb2Marshaller;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.util.ClassUtils;
//import org.springframework.ws.client.core.WebServiceTemplate;
//
//import com.sap.document.sap.soap.functions.mc_style.ZishGetPatientApp;
//
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
//public class SoapIntegrationTest {
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(SoapIntegrationTest.class);
//	
//	@Value("${sap.wsdl.url}")
//    private String wsdlUrl;
//	
//    private Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
//
//    @LocalServerPort
//    private int port = 0;
//
//    @Before
//    public void init() throws Exception {
//        marshaller.setPackagesToScan(ClassUtils.getPackageName(ZishGetPatientApp.class));
//        marshaller.afterPropertiesSet();
//    }
//
//    @Test
//    public void testSendAndReceive() {
//    	LOGGER.info("INICIANDO PRUEBAS DE SOAP");
//    	
//        WebServiceTemplate ws = new WebServiceTemplate(marshaller);
//        ZishGetPatientApp request = new ZishGetPatientApp();
//        request.setICentro("MICROCEN");
//        request.setINrodoc("DNI");
//        request.setITipodoc("32669524");
//
//        LOGGER.info("CARGADOS LOS VALORES");
//        
//        assertThat(ws.marshalSendAndReceive(wsdlUrl, request)).isNotNull();
//        LOGGER.info("FIN??");
//    }
//
//}
