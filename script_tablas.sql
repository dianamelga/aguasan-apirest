-- DROP TABLES
DROP TABLE "public"."ciudad" CASCADE;
DROP TABLE "public"."usuario_acceso" CASCADE;
DROP TABLE "public"."ubicaciones_tipos" CASCADE;
DROP TABLE "public"."ubicaciones_clientes" CASCADE;
DROP TABLE "public"."barrio" CASCADE;
DROP TABLE "public"."tipo_documento" CASCADE;
DROP TABLE "public"."clientes" CASCADE;
DROP TABLE "public"."usuario_acceso_auditoria" CASCADE;
DROP TABLE "public"."solicitud_accion" CASCADE;
DROP TABLE "public"."usuarios" CASCADE;


DROP SEQUENCE serial;

CREATE SEQUENCE serial
	START 1;

CREATE TABLE ciudad (
	cod_ciudad varchar(4) NOT NULL,
	ciudad varchar(40) NOT NULL
);

ALTER TABLE ciudad
	ADD CONSTRAINT pk_ciudad PRIMARY KEY (cod_ciudad);

CREATE TABLE barrio (
	cod_barrio varchar(4) NOT NULL,
	barrio varchar(40) NOT NULL,
	cod_ciudad varchar(4) NOT NULL
);

ALTER TABLE barrio
	ADD CONSTRAINT pk_barrio PRIMARY KEY (cod_barrio, cod_ciudad);

ALTER TABLE barrio
	ADD CONSTRAINT fk_ciudad FOREIGN KEY (cod_ciudad) REFERENCES ciudad (cod_ciudad);

CREATE TABLE tipo_documento (
	cod_tipo_documento varchar(4) NOT NULL,
	tipo_documento varchar(40) NOT NULL
);

ALTER TABLE tipo_documento
	ADD CONSTRAINT pk_tipo_documento PRIMARY KEY (cod_tipo_documento);

CREATE TABLE usuarios (
	username varchar(8) NOT NULL,
	PASSWORD varchar(40) NOT NULL,
	nombre varchar(60) NOT NULL,
	cod_tipo_documento varchar(4) NOT NULL,
	nro_documento varchar(15) NOT NULL,
	email varchar(60),
	es_admin char(1) NOT NULL,
	activo char(1) NOT NULL,
	fec_alta date NOT NULL,
	fec_ult_act date NOT NULL
);

ALTER TABLE usuarios
	ADD CONSTRAINT pk_usuarios PRIMARY KEY (username);

ALTER TABLE usuarios
	ADD CONSTRAINT fk_usu_tipo_doc FOREIGN KEY (cod_tipo_documento) REFERENCES tipo_documento (cod_tipo_documento);

CREATE TABLE usuario_acceso (
	username varchar(8) NOT NULL,
	device_id varchar(80) NOT NULL,
	serial_instal integer NOT NULL,
	fec_ult_act date NOT NULL
);

ALTER TABLE usuario_acceso
	ADD CONSTRAINT pk_usuario_acceso PRIMARY KEY (username);

CREATE TABLE usuario_acceso_auditoria (
	username varchar(8) NOT NULL,
	device_id varchar(80) NOT NULL,
	serial_instal integer NOT NULL,
	fec_ult_act date NOT NULL
);

CREATE TABLE clientes (
	nro_formulario integer NOT NULL,
	cod_cliente SERIAL NOT NULL,
	nombres varchar(60) NOT NULL,
	apellidos varchar(60) NOT NULL,
	celular varchar(15) NOT NULL,
	telefono varchar(15),
	cod_tipo_documento varchar(4) NOT NULL,
	nro_documento varchar(15) NOT NULL,
	cod_ciudad varchar(4) NOT NULL,
	cod_barrio varchar(4) NOT NULL,
	direccion varchar(200) NOT NULL,
	observacion varchar(200) NOT NULL,
	dias_visita varchar(200) NOT NULL,
	bidones integer NOT NULL,
	dispensers integer NOT NULL,
	bebederos integer NOT NULL,
	fec_alta date NOT NULL,
	usr_alta varchar(8) NOT NULL,
	fec_ult_act date NOT NULL,
	usr_ult_act varchar(8) NOT NULL,
	activo varchar(4) NOT NULL
);

ALTER TABLE clientes
	ADD CONSTRAINT pk_clientes PRIMARY KEY (cod_cliente);

ALTER TABLE clientes
	ADD CONSTRAINT fk_cod_tipo_documento FOREIGN KEY (cod_tipo_documento) REFERENCES tipo_documento (cod_tipo_documento);


ALTER TABLE clientes
	ADD CONSTRAINT fk_barrio FOREIGN KEY (cod_barrio, cod_ciudad) REFERENCES barrio (cod_barrio, cod_ciudad);

ALTER TABLE usuarios
	ADD CONSTRAINT fk_cli_tipo_doc FOREIGN KEY (cod_tipo_documento) REFERENCES tipo_documento (cod_tipo_documento);

CREATE TABLE ubicaciones_tipos (
	cod_tipo_ubicacion SERIAL NOT NULL,
	tipo_ubicacion varchar(40) NOT NULL,
	fec_alta date NOT NULL,
	usr_alta varchar(8) NOT NULL,
	fec_ult_act date NOT NULL,
	usr_ult_act varchar(8) NOT NULL
);

ALTER TABLE ubicaciones_tipos
	ADD CONSTRAINT pk_ubicaciones_tipos PRIMARY KEY (cod_tipo_ubicacion);

CREATE TABLE ubicaciones_clientes (
	cod_ubicacion SERIAL NOT NULL,
	cod_tipo_ubicacion integer NOT NULL,
	cod_cliente integer NOT NULL,
	latitud varchar(100) NOT NULL,
	longitud varchar(100) NOT NULL,
	fec_alta date NOT NULL,
	usr_alta varchar(8) NOT NULL,
	fec_ult_act date NOT NULL,
	usr_ult_act varchar(8) NOT NULL
);

ALTER TABLE ubicaciones_clientes
	ADD CONSTRAINT pk_ubicaciones_clientes PRIMARY KEY (cod_ubicacion);

ALTER TABLE ubicaciones_clientes
	ADD CONSTRAINT fk_ubi_cli_tipo_ubi FOREIGN KEY (cod_tipo_ubicacion) REFERENCES ubicaciones_tipos (cod_tipo_ubicacion);

ALTER TABLE ubicaciones_clientes
	ADD CONSTRAINT fk_ubi_cli_clientes FOREIGN KEY (cod_cliente) REFERENCES clientes (cod_cliente);

CREATE TABLE solicitud_accion (
	id_accion SERIAL NOT NULL,
	tipo varchar(4) NOT NULL,
	/* ADD, UPD, DEL*/
	objeto_json varchar(1000) NOT NULL,
	estado varchar(20) NOT NULL,
	id_remoto integer NOT NULL,
	serial_instal integer NOT NULL,
	fec_alta date NOT NULL,
	usr_alta varchar(8) NOT NULL,
	fec_ult_act date NOT NULL,
	usr_ult_act varchar(8) NOT NULL
);

ALTER TABLE solicitud_accion
	ADD CONSTRAINT pk_solicitud_accion PRIMARY KEY (id_accion);

ALTER TABLE solicitud_accion
	ADD CONSTRAINT fk_usr_alta FOREIGN KEY (usr_alta) REFERENCES usuarios (username);

ALTER TABLE solicitud_accion
	ADD CONSTRAINT fk_usr_utl_act FOREIGN KEY (usr_ult_act) REFERENCES usuarios (username);
	
	
INSERT INTO ciudad(cod_ciudad, ciudad) VALUES('1','Luque');
INSERT INTO ciudad(cod_ciudad, ciudad) VALUES('2','Asuncion');
INSERT INTO barrio(cod_barrio, barrio, cod_ciudad) VALUES('1','Bella Vista','1');
INSERT INTO barrio(cod_barrio, barrio, cod_ciudad) VALUES('2','Las Mercedes','2');
INSERT INTO "public"."tipo_documento" ("cod_tipo_documento", "tipo_documento") VALUES ('CIP', 'Cedula de Identidad');
INSERT INTO "public"."tipo_documento" ("cod_tipo_documento", "tipo_documento") VALUES ('PSP', 'Pasaporte');

INSERT INTO usuarios(
	username, password, nombre, cod_tipo_documento, nro_documento, 
	email, es_admin, activo, fec_alta, fec_ult_act)	
	VALUES ('diana', '123456', 'Diana Melgarejo', 'CIP', '4484526',
	'di.melgarejo@gmail.com', 'S', 'S', date('now'), date('now'));
